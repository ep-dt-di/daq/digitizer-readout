/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/

/*!
 * \file vx1730_Run.cpp
 *
 * \author Sam Meehan
 * \date 13 Jan 2021
 *
 * \brief Standalone executable
 *
 * Standalone executable to run the digitizer
 */


#include "vx1730_Run.h"

int main(int argc, char *argv[])
{

  // for catching the exit 
  signal(SIGINT, my_handler);

  // used to tag the name of the program if you want in the output file
  std::string programName = argv[0];
  std::cout<<"Program Running : "<<programName<<std::endl;
  std::size_t pos = programName.find("/");
  std::string programTag = programName.substr(pos+1);
  std::cout<<"Program Tag     : "<<programTag<<std::endl;

  ////////////////////////////////////////
  // Parse input arguments
  ////////////////////////////////////////
  
  // require that you point the program at a config file
  // which will at least be used for the ip address and local base address
  if(argc<=1){
    std::cout<<"********************************************"<<std::endl;
    std::cout<<"Incorrect Usage : Need at least a --runtype argument to "<<std::endl;
    std::cout<<"to tell the program which functionality to execute"<<std::endl;
    std::cout<<"Please check usage via the [-h] help option"<<std::endl;
    std::cout<<"********************************************"<<std::endl;
    return 1;
  }
  
  // get optional arguments from parser
  // https://www.geeksforgeeks.org/getopt-function-in-c-to-parse-command-line-arguments/
  // https://stackoverflow.com/questions/8793020/using-getopt-long-c-how-do-i-code-up-a-long-short-option-to-both-require-a
  
  const struct option longopts[] =
  {
    {"help",    no_argument, 0, 'h'},
    {"runtype", required_argument, 0, 'r'},
    {"config",  required_argument, 0, 'c'},
    {"output",  required_argument, 0, 'o'},
    {"force",   no_argument, 0, 'f'},
    {"debug",   no_argument, 0, 'd'},
    {"nevents", required_argument, 0, 'n'},
    {0,0,0,0}
  };
  
  int index;
  int iarg=0;
  char *rType = NULL;
  char *cPath = NULL;
  char *oPath = NULL;
  bool forceRemove = false;
  bool debug = false;
  int  nAcquire = -1;
  
  while((iarg = getopt_long(argc, argv, ":h:r:c:o:f:d", longopts, &index)) != -1)
  {
    switch (iarg)
    {
      case 'h':
        std::cout<<std::endl<<std::endl;
        std::cout<<"This is the help screen for your vx1730 digitizer command line running"<<std::endl<<std::endl;
        std::cout<<"Synopsis : ./vx1730_Run -r --runtype [RUNTYPE]"<<std::endl;
        std::cout<<"Optional arguments : "<<std::endl;
        std::cout<<" -c --config    [CONFIGFILE] : CONFIGFILE is the path to the json formatted config file"<<std::endl;
        std::cout<<" -o --output    [OUTPUTPATH] : OUTPUTPATH is the location of the output file"<<std::endl;
        std::cout<<" -f --force                  : If set, this will remove the output file"<<std::endl;
        std::cout<<" -d --debug                  : If set, this will cause debug flag to be set which is used throughout"<<std::endl;
        std::cout<<" -n --nevents   [NEVENTS]    : NEVENTS is the number of events that will be triggered before dumping the data and exitting"<<std::endl;
        
        std::cout<<"Example : How to ping the module to check its connectivity"<<std::endl;
        std::cout<<"          cmd-prompt> ./vx1730_Run --runtype showconfig --config ../configs/cfg_vx1730.json"<<std::endl;
        std::cout<<std::endl;
        return 1;
        break;
      case 'r':
        std::cout << "Argument : Run type = " << optarg << std::endl;
        rType = optarg;
        break;
      case 'c':
        std::cout << "Argument : Config file = " << optarg << std::endl;
        cPath = optarg;
        break;
      case 'o':
        std::cout << "Argument : Output file = " << optarg << std::endl;
        oPath = optarg;
        break;
      case 'f':
        std::cout << "Argument : Force remove" << std::endl;
        forceRemove=true;
        break;
      case 'd':
        std::cout << "Argument : Debug flag set" << std::endl;
        debug = true;
        break;
      case 'n':
        std::cout << "Argument : NEvents to acquire" << optarg << std::endl;
        nAcquire = std::stoi(optarg);
        break;
      case '?':
        std::cout<<"Argument : Unknown argument option = " << optopt << std::endl;
        break;      
    }
  }
  
  // output directory for running
  std::string outputPath;
  if(oPath){
    outputPath = oPath;
    outputPath += "/";
    std::cout<<"\n\nOutput Path : "<<outputPath<<std::endl;
    int dir_err = 0;

    // if enabled you will remove the output directory to start freshly
    if(forceRemove){
      dir_err = system( (std::string("rm -r ")+outputPath).c_str() );
      if(dir_err == -1){
        std::cout<<"Error removing directory : "<<outputPath<<std::endl;
        return 1;
      }
    }

    // creation of output directory
    dir_err = system( (std::string("mkdir -p ")+outputPath).c_str() );
    if(dir_err == -1){
      std::cout<<"Error creating directory : "<<outputPath<<std::endl;
      return 1;
    }
  }
  
  ////////////////////////////////////////
  // Get json configuration file
  //
  // In DAQling, this is done behind the scenes and is accessed in a module as
  // auto cfg = m_config.getConfig()["settings"];
  // this is because the input configuration here is only the "settings" of the 
  // DigitizerModule
  ////////////////////////////////////////
  
  // open the json config file
  std::string configPath(cPath);
  std::cout<<"\n\nConfig Path : "<<configPath<<std::endl;
  json myConfig = openJsonFile(configPath.c_str());
  std::cout<<"this"<<std::endl;
  
  ////////////////////////////////////////
  // Get the Communication chain
  // - IP address of interface board
  // - Digitizer hardware address in crate
  ////////////////////////////////////////
  
  // ip address
  char input_ip_location[32];
  strcpy(input_ip_location, std::string(myConfig["host_pc"]).c_str() ) ;

  std::string ip_str = std::string(GetIPAddress(input_ip_location));

  char ip_addr_string[32];
  strcpy(ip_addr_string, std::string(GetIPAddress(input_ip_location)).c_str() );
  INFO("\nReturned IP Address : "<<*ip_addr_string);

  // vme base address
  INFO("Getting VME base : "<<myConfig["digitizer_hw_address"]);
  std::string vme_base_address_str = std::string(myConfig["digitizer_hw_address"]);
  UINT vme_base_address = std::stoi(vme_base_address_str,0,16);
  INFO("Base VME Address = 0x"<<std::setfill('0')<<std::setw(8)<<std::hex<<vme_base_address);

  ////////////////////////////////////////
  // Load up the software instance of the digitizer control
  // 
  // This opens up the communication channels to the hardware itself
  ////////////////////////////////////////

  // make a new digitizer instance
  m_digitizer = new vx1730(ip_addr_string, vme_base_address);
  
  // test digitizer board interface
  m_digitizer->TestCommInterface(myConfig);  
  
  // test digitizer board interface
  m_digitizer->TestCommDigitizer(myConfig);
  
  // configuring ethernet transmission settings for interface board  
  // NOTE : you also need to ensure that the utm setting on ifconfig is sufficiently high for jumbo frames to work
  bool set_interface_jumbo = (bool)myConfig["readout"]["interface_jumbo"];
  m_digitizer->SetInterfaceEthernetJumboFrames(set_interface_jumbo);
  
  unsigned int set_interface_packet_gap = (unsigned int)myConfig["readout"]["interface_packet_gap"];
  m_digitizer->SetInterfaceEthernetGap(set_interface_packet_gap);

  unsigned int set_interface_max_packets = (unsigned int)myConfig["readout"]["interface_max_packets"];
  m_digitizer->SetInterfaceEthernetMaxPackets(set_interface_max_packets);

  // ethernet speed test
  // these can be removed if the bootup is too slow
  // gives nwords/second --> need to translate to MB/s
  float interface_rate     = m_digitizer->PerformInterfaceSpeedTest(4);

  DEBUG("Speed(interface)     [words/s]: "<<interface_rate    );
  DEBUG("Speed(interface)     [MB/s]: "<<(interface_rate*4)/1000000.     );


  /*
  for(int i=0; i<100; i++){
    auto start_loop_time = chrono::high_resolution_clock::now();
    //    m_digitizer->TestComm();
    float interface_rate     = m_digitizer->PerformInterfaceSpeedTest(4);
    // system time after
    auto end_loop_time = chrono::high_resolution_clock::now();
    //    float time_for_loop = end_loop_time - start_loop_time;
    float time_loop_time = chrono::duration_cast<chrono::nanoseconds>(end_loop_time - start_loop_time).count() * 1e-9;
    DEBUG("Time taken by TestComm: " << time_loop_time);
  }
  */


  ////////////////////////////////////////
  // Execute the run for the chosen configuration type
  ////////////////////////////////////////
  if( strcmp(rType,"showconfig")==0 ){
    INFO("Dumping the Current Config");
    m_digitizer->Configure(myConfig, true);
    m_digitizer->DumpConfig();
    INFO("NEventsInBuffer : "<<m_digitizer->DumpEventCount(false));
  }
  else if( strcmp(rType,"stoprun")==0 ){
    INFO("Stopping run after clearing buffer");
    m_digitizer->StopAcquisition(true);
    std::cout<<"NEvents BeforeRead : "<<std::dec<<m_digitizer->DumpEventCount( false )<<std::endl;
    while(m_digitizer->DumpEventCount( false )){
      std::cout<<"NEv1 : "<<m_digitizer->DumpEventCount( false )<<std::endl;
      m_digitizer->DumpFrontEvent( "delme.txt", DumpMode::New, true);
      std::cout<<"NEv2 : "<<m_digitizer->DumpEventCount( false )<<std::endl;
    }
    std::cout<<"NEvents AfterRead : "<<std::dec<<m_digitizer->DumpEventCount( false )<<std::endl;
  }
  else if( strcmp(rType,"waitfortrigger")==0 ){
    INFO("Waiting for triggers (for things like TLB testing)");
    m_digitizer->Configure(myConfig, true);  
    m_digitizer->StartAcquisition(true);
    
    int count=0;
    while(true){
      std::cout<<"Waiting : "<<std::dec<<count<<" - exit with CTRL+C"<<std::endl;
      count++;
      Wait(1.0);
    }
    
    m_digitizer->StopAcquisition(true);
  }
  else if( strcmp(rType,"swtrigger")==0 ){
    INFO("Getting SW triggers");
    m_digitizer->Configure(myConfig, true);  
    m_digitizer->StartAcquisition(true);
    
    // check that the input arguments align with what is expected
    if(nAcquire==-1){
      std::cout<<"You didn't provide a number of events to acquire"<<std::endl;
      return 9;
    }
    
    for(int iEv=0; iEv<nAcquire; iEv++){
      INFO("Count : "<<iEv);
      Wait(1.0);
      m_digitizer->SendSWTrigger();
      
      INFO("Get the Event");
      uint32_t raw_payload[MAXFRAGSIZE];
      m_digitizer->ReadEventSingle( raw_payload, true );
    }
    m_digitizer->StopAcquisition(true);
  }
  else if( strcmp(rType,"takedata")==0 ){
    INFO("Acquire N events, where N is configurable");
    
    // check that the input arguments align with what is expected
    if(nAcquire==-1){
      std::cout<<"You didn't provide a number of events to acquire"<<std::endl;
      return 9;
    }
    if(oPath==NULL){
      std::cout<<"You didn't provide an output path argument "<<std::endl;
      return 9;
    }
        
    INFO("CONFIG before configuration");
    m_digitizer->DumpConfig();
    
    m_digitizer->Configure(myConfig, true);  
    
    INFO("CONFIG before startAcquire");
    m_digitizer->DumpConfig();
    
    m_digitizer->StartAcquisition(true);
    
    INFO("CONFIG after startacquire");
    m_digitizer->DumpConfig();
    
    INFO("The m_digitizer is now acquiring data ...");

    while( m_digitizer->DumpEventCount() < nAcquire ){
      std::cout<<"Acquired NEvents = "<<m_digitizer->DumpEventCount()<<"  Target = "<<nAcquire<<std::endl;
      std::cout<<"Waiting for 1 second"<<std::endl;
      Wait(1);
    }
    
    m_digitizer->StopAcquisition(true);
  
  
    WARNING("Writing data out until event buffer is empty");
    std::cout<<"The current event count is : "<<m_digitizer->DumpEventCount()<<std::endl;
  
    // single output file specified with -o input
    std::string outputFile = outputPath+"data.txt";
    std::cout<<"Writing data to : "<<outputFile<<std::endl;
    ofstream outfile;
    outfile.open(outputFile, ios::out);
    outfile<<"New Data Run : "<<GetDateVerboseString()<<std::endl<<std::endl;
    outfile.close();
 
    // keep looping while there are events in the buffer and append them
    while(m_digitizer->DumpEventCount() != 0){
      m_digitizer->DumpFrontEvent( outputFile, DumpMode::Append );
      std::cout<<"Events left to read : "<<m_digitizer->DumpEventCount()<<std::endl;
    }
    std::cout<<"The final event count is (should be 0) : "<<m_digitizer->DumpEventCount()<<std::endl;

    WARNING("The run and data readout should be finished");

  }
  else if(strcmp(rType,"daqpy")==0 ){
    INFO("Rate Readout");
    
    // configure()
    INFO("CONFIG before configuration");
    m_digitizer->DumpConfig();
    
    m_digitizer->Configure(myConfig, true);  
    
    INFO("CONFIG before startAcquire");
    m_digitizer->DumpConfig();
    
    DEBUG("Enter any number to begin data taking (review config)");
    int xstart;
    std::cin>>xstart;
    
    // start()
    m_digitizer->StartAcquisition(true);
    INFO("The m_digitizer is now acquiring data ...");
    
    
    // runner()
    // these would normally be initialized elsewhere
    m_triggers = 0;
    m_ECRcount = 64;
    m_ttt_converter = 64;
    m_ttt_bcid_fix = 37.6;

    // for timed running
    auto start = chrono::steady_clock::now();

    DEBUG("Allocating software buffer ...");
    int software_buffer = (int)myConfig["readout"]["software_buffer"];           // Size needed for array
    DEBUG("NBuffer ..."<<std::dec<<software_buffer);

    unsigned int* raw_payload = NULL;   // Pointer to int, initialize to nothing.
    raw_payload = new unsigned int[software_buffer];  // Allocate n ints and save ptr in a.
    
    
    // initializing other variables once
    int nchannels_enabled = 0;
    for(int iChan=0; iChan<16; iChan++){
      if((int)myConfig["channel_readout"].at(iChan)["enable"]==1)
        nchannels_enabled++;
    }
    int buffer_size = m_digitizer->RetrieveBufferLength();
    
    std::string readout_method = (std::string)myConfig["readout"]["readout_method"];
    int readout_blt            = (int)myConfig["readout"]["readout_blt"];
    
    // for SW trigger sending
    bool swtrigger_send = (bool)myConfig["trigger_software"]["enable"];
    int swtrigger_rate  = (int)myConfig["trigger_software"]["rate"];

    float time_loop_time = 1;

    // stores monitoring information to be written to histograms/rates
    std::map<std::string, float> monitoring;
    
    // request the BLT readout rate
    int n_events_requested = readout_blt;
    
    // from here onwards it should be like runner()
    // while(m_run) loop in DigitizerReceiver
    while(true){
      
      // system time after
      auto start_loop_time = chrono::high_resolution_clock::now(); 
    
      // send sw triggers if enabled
      int nsend = swtrigger_rate;
      for(int itrig=0; itrig<nsend; itrig++){
	m_digitizer->SendSWTrigger();
      }

      // lock to prevent accidental double reading with the sendECR() call
      m_lock.lock();
      
      int n_events_parsed;
    
      // polling of the hardware - if any number of events is detected
      // then all events in the buffer are read out
      int n_events_present = m_digitizer->DumpEventCount();
      DEBUG("NEvents Found : "<<n_events_present);
      if(n_events_present){
        DEBUG("Sending all events - NEvents = "<<std::dec<<n_events_present);
        
        // clear the buffer readout monitoring map        
        monitoring.clear();
        
        // new method        
        // get the data from the board into the software buffer        
        int nevents_obtained = 0;
        nevents_obtained = m_digitizer->RetrieveEventBatch(raw_payload, software_buffer, monitoring, n_events_present, nchannels_enabled, buffer_size, readout_method, n_events_requested, m_ECRcount, m_ttt_converter);

        // parse the events and decorate them with a FASER header
        std::vector<EventFragment> fragments;
        fragments = m_digitizer->ParseEventBatch(raw_payload, software_buffer, nevents_obtained, monitoring, n_events_present, nchannels_enabled, buffer_size, readout_method, n_events_requested, m_ECRcount, m_ttt_converter, m_ttt_bcid_fix);
        
        n_events_parsed = fragments.size();
        
        // pass the group of events on in faser/daq
        // NOTE : this should only exist in the DigitizerReceiver module due to access
        //        of the DAQling methods and such
        // passEventBatch(fragments) // defined in the Digitizer
        
        if((bool)myConfig["readout"]["print_event"]){
          if(fragments.size()>=1){
            const EventFragment frag = fragments.at(0);
            DigitizerDataFragment digitizer_data_frag = DigitizerDataFragment(frag.payload<const uint32_t*>(), frag.payload_size());
            std::cout<<"Digitizer data fragment:"<<std::endl;
	    std::cout<<"Word[0] : "<<ConvertIntToWord(raw_payload[0])<<std::endl;
	    std::cout<<"Word[1] : "<<ConvertIntToWord(raw_payload[1])<<std::endl;
	    std::cout<<"Word[2] : "<<ConvertIntToWord(raw_payload[2])<<std::endl;
	    std::cout<<"Word[3] : "<<ConvertIntToWord(raw_payload[3])<<std::endl;
	    std::cout<<digitizer_data_frag<<std::endl;
	    
	  }
        }
        
        m_lock.unlock();
        DEBUG("Total nevents sent in running : "<<m_triggers);
      }
      else{
        // sleep for 1.5 milliseconds. This time is chosen to ensure that the polling 
        // happens at a rate just above the expected trigger rate in the case of no events
        //DEBUG("No events - sleeping for a short while");
        m_lock.unlock();
        usleep(100); 
      }
      
      // system time after
      auto end_loop_time = chrono::high_resolution_clock::now(); 

      time_loop_time = chrono::duration_cast<chrono::nanoseconds>(end_loop_time - start_loop_time).count() * 1e-9; 
      DEBUG("Time taken by looping is : " << fixed << time_loop_time << setprecision(5) << " sec ");
      
      monitoring["time_looping_time"] = time_loop_time;
   
      DEBUG("Time Retrieve  : "<<monitoring["time_read_time"]);
      DEBUG("Time Parse     : "<<monitoring["time_parse_time"]);
      DEBUG("Time AddHeader : "<<monitoring["time_header_time"]);
      DEBUG("Time Overhead  : "<<monitoring["time_filler_time"]);
      
      float total_batch = monitoring["time_read_time"]+monitoring["time_parse_time"]+monitoring["time_header_time"]+monitoring["time_filler_time"];
      
      DEBUG("Parse/Read   : "<<monitoring["time_parse_time"]/monitoring["time_read_time"]);
      
      DEBUG("Total batch  : "<<total_batch);
      DEBUG("Time looping : "<<monitoring["time_looping_time"]<<"   ("<<total_batch/monitoring["time_looping_time"]<<")");
      
      DEBUG("NEventsParsed : "<<n_events_parsed);

      float rate = n_events_parsed/total_batch;
      DEBUG("ApproximateRate : "<<rate);
      
    }

    // stop event acquisition to exit cleanly
    m_digitizer->StopAcquisition(true);    
  }
  else if( strcmp(rType,"dev")==0 ){
    INFO("DEVELOPMENT");
  }
  else{
    ERROR("Not a valid runtype : "<<rType);
  }
  
  return 0;
}
