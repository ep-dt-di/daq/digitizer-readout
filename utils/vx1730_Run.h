/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/

/*!
 * \file vx1730_Run.h
 *
 * \author Sam Meehan
 * \date 13 Jan 2021
 *
 * \brief Header for standalone executable
 *
 * 
 */

#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <stdlib.h>

#include "Helper.h"
#include "Helper_Event.h"
#include "Helper_sis3153.h"

#include "Comm_vx1730.h"

#include "nlohmann/json.hpp"
using json = nlohmann::json;

#include <unistd.h>
#include <iostream>
#include <cstdlib>
#include <signal.h>
#include <getopt.h>

// needed for sendECR() and runner() protection
#include <mutex>

// for measuring time
#include <chrono>

// this searches for an exit via 
void my_handler(int s){
  printf("********************************\n");
  printf("STOP It!!!! : Caught signal %d\n",s);
  printf("********************************\n");
  exit(1); 
}

// for argument parsing
#define no_argument 0
#define required_argument 1 
#define optional_argument 2

// to have single global digitizer object like in daqpy module
vx1730 *m_digitizer;

// used to protect against the ECR and runner() from reading out at the same time
std::mutex m_lock;

int m_triggers;
int m_ECRcount;
int m_ttt_converter;
float m_ttt_bcid_fix;