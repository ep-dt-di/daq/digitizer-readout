/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/

/*!
 * \file CalibrateADC.cpp
 *
 * \author Sam Meehan
 * \date 13 Jan 2021
 *
 * \brief Calibrate ADCs for temperature swings
 *
 * \details Executable for performing calibration of ADC chips on vx1730 board
 */

#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <stdlib.h>

#include "Helper.h"
#include "Helper_Event.h"
#include "Helper_sis3153.h"

#include "Comm_vx1730.h"

#include "nlohmann/json.hpp"
using json = nlohmann::json;

#include <unistd.h>
#include <iostream>
#include <cstdlib>
#include <signal.h>
#include <getopt.h>

// for measuring time
#include <chrono>

// for argument parsing
#define no_argument 0
#define required_argument 1 
#define optional_argument 2




int main(int argc, char *argv[])
{

  // used to tag the name of the program if you want in the output file
  std::string programName = argv[0];
  std::cout<<"Program Running : "<<programName<<std::endl;
  std::size_t pos = programName.find("/");
  std::string programTag = programName.substr(pos+1);
  std::cout<<"Program Tag     : "<<programTag<<std::endl;

  ////////////////////////////////////////
  // Parse input arguments
  ////////////////////////////////////////
  
  // require that you point the program at a config file
  // which will at least be used for the ip address and local base address
  if(argc<=1){
    std::cout<<"********************************************"<<std::endl;
    std::cout<<"Incorrect Usage : "<<std::endl;
    std::cout<<"Please check usage via the [-h] help option"<<std::endl;
    std::cout<<"********************************************"<<std::endl;
    return 1;
  }
  
  // get optional arguments from parser
  // https://www.geeksforgeeks.org/getopt-function-in-c-to-parse-command-line-arguments/
  // https://stackoverflow.com/questions/8793020/using-getopt-long-c-how-do-i-code-up-a-long-short-option-to-both-require-a
  
  const struct option longopts[] =
  {
    {"help",    no_argument, 0, 'h'},
    {"ip", required_argument, 0, 'i'},
    {"rotary", required_argument, 0, 'r'},
    {0,0,0,0}
  };
  
  int index;
  int iarg=0;
  char *ipAddress     = NULL;
  char *rotaryAddress = NULL;
  
  while((iarg = getopt_long(argc, argv, ":h:i:r:", longopts, &index)) != -1)
  {
    switch (iarg)
    {
      case 'h':
        std::cout<<std::endl<<std::endl;
        std::cout<<"This is the help screen for your ADC calibration"<<std::endl<<std::endl;
        std::cout<<"Usage : ./CalibrateADC -i <IP Address> -r <RotarySwitch>"<<std::endl;
        std::cout<<std::endl;
        return 1;
        break;
      case 'i':
        std::cout << "Argument : IP Address = " << optarg << std::endl;
        ipAddress = optarg;
        break;
      case 'r':
        std::cout << "Argument : Rotary Address = " << optarg << std::endl;
        rotaryAddress = optarg;
        break;
      case '?':
        std::cout<<"Argument : Unknown argument option = " << optopt << std::endl;
        std::cout<<"Please see usage with [-h] ... exitting and not running"<<std::endl;
        return 2;
        break;      
    }
  }
  
  // ip address
  char input_ip_location[32];
  strcpy(input_ip_location, ipAddress) ;
  
  std::string ip_str = std::string(GetIPAddress(input_ip_location));
  

  char ip_addr_string[32];
  strcpy(ip_addr_string, std::string(GetIPAddress(input_ip_location)).c_str() );
  INFO("Returned IP Address : "<<ip_addr_string);

  // vme base address  
  char input_rotary_address[32];
  strcpy(input_rotary_address, rotaryAddress) ;
  INFO("Input : "<<input_rotary_address);
  std::string vme_base_address_str = std::string(input_rotary_address);
  
  if(vme_base_address_str.size()!=10){
    ERROR("You don't seem to have provided a correctly formatted HW address");
    ERROR("It should be of the format 0x12345678");
    return 2;
  }


  if(!(vme_base_address_str[0]=='0' && vme_base_address_str[1]=='x')){
    ERROR("You don't seem to have provided a correctly formatted HW address");
    ERROR("It should be of the format 0x12345678");
    return 2;
  }

  bool found_x=false;

  for (int i = 0; i < vme_base_address_str.size(); i++){
    //std::cout << vme_base_address_str[i]<<std::endl;
    if(vme_base_address_str[i]=='x'){
      if(found_x==true){
	ERROR("You don't seem to have provided a correctly formatted HW address");
	ERROR("It should be of the format 0x12345678");
	return 2;
      }
      found_x = true;
    }
    else if(!(vme_base_address_str[i]=='0' || 
	      vme_base_address_str[i]=='1' ||
	      vme_base_address_str[i]=='2' ||
	      vme_base_address_str[i]=='3' ||
	      vme_base_address_str[i]=='4' ||
	      vme_base_address_str[i]=='5' ||
	      vme_base_address_str[i]=='6' ||
	      vme_base_address_str[i]=='7' ||
	      vme_base_address_str[i]=='8' ||
	      vme_base_address_str[i]=='9')){
	      ERROR("You don't seem to have provided a correctly formatted HW address");
	      ERROR("It should be of the format 0x12345678");
	      return 2;
	    }
  }

  if(found_x==false){
    ERROR("You don't seem to have provided a correctly formatted HW address");
    ERROR("It should be of the format 0x12345678");
    return 2;
  }


  UINT vme_base_address = std::stoi(vme_base_address_str,0,16);
  INFO("IntValue : "<<vme_base_address);
  INFO("Base VME Address = 0x"<<std::setfill('0')<<std::setw(8)<<std::hex<<vme_base_address);

  ////////////////////////////////////////
  // Load up the software instance of the digitizer control
  // This opens up the communication channels to the hardware itself
  ////////////////////////////////////////

  // make a new digitizer instance
  vx1730 *m_digitizer = new vx1730(ip_addr_string, vme_base_address);
  
  // test digitizer board interface
  sis3153_TestComm( m_digitizer->m_crate, true);
  
  INFO("Resetting board configuration");
  m_digitizer->Reset();

  // wait one second for temperature to stabilize if just turned on
  // documentation proposes at lease 100ms so this is fine
  INFO("Waiting for temperature to stabilize");
  Wait(1.0);
  
  // write to register that resets 
  INFO("Performing calibration");
  m_digitizer->ADCCalibration();
  
  int status[16]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
  
  while(true){
    INFO("Continuing calibration ... channel status [0 = not finished, 1 = finished] : ");
    
    bool calibration_finished=true;
    
    for(int ich=0; ich<16; ich++){
      UINT data;
      ReadSlaveReg(m_digitizer->m_crate, m_digitizer->m_base_address + VX1730_CHANNEL_STATUS + (0x0100)*ich, data, false );
      
      INFO("ch "<<ich<<" : "<<GetWordBit(data,3));
      
      if(GetWordBit(data,3)==0){
        calibration_finished=false;
      }
      
    }
    
    if(calibration_finished==true){
      break;
    }

    //    Wait(1.0);
  }

  INFO("Calibration finished");
  
  return 0;
}
