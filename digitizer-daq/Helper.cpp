/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/

/*!
 * \file Helper.cpp
 * \copyright Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
 * \author Sam Meehan
 * \date 13 Jan 2021
 *
 * \brief Helper utilities for code
 *
 */
 
#include "Helper.h"
#include <cstring>

/*!
 * \brief Load a json file
 *
 * Opens a text file that contains a json object and loads it into memory
 *
 * \param[in] [filepath] Path to the text file containing the json object
 * \return json object that can be navigated using normal calls to the nlohmann json library
 *
 */
json openJsonFile(std::string filepath){
  INFO("Openning config : "<<filepath);
  std::ifstream file(filepath);
  if (!file) {
    throw std::runtime_error("Could not open json config file : "+filepath);
  }
  json j;
  try {
    j = json::parse(file);
  }catch (json::parse_error &e) {
    throw std::runtime_error(e.what());
  }
  file.close();
  return j;
}

/*!
 * \brief Convert word to string
 *
 * Converts a 32 bit word into a human interpretable binary format so that it is easier to print and determine the value of a specific bit
 *
 * \param[in] [word] The 32 bit word to convert
 * \param[in] [debug] A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.
 * \return String formatted as eight sets of four bit sequences from bit[31,0]
 *
 */
std::string ConvertIntToWord(unsigned int word, bool debug){
  // converts an unsigned int to a string of 0's and 1's formatted
  // in an appropriate manner as a 32 bit word

  std::string wordout;

  for(int iBit=31; iBit>=0; iBit--){

    int bitval = (word & (1<<iBit)) >> iBit;

    if(debug)
      INFO("bitval : "<<std::dec<<iBit<<"  "<<bitval);

    wordout += std::to_string(bitval);

    if(iBit%4==0)
      wordout += " ";

  }

  if(debug)
    INFO("wordout : "<<wordout);

  return wordout;
}

/*!
 * \brief Convert single bit in word
 *
 * Sets the value of a single bit in a word to a specific value
 *
 * \param[out] [word] The word that is to be manipulated
 * \param[in]  [bit_location] The bit location that you want to manipulate
 * \param[in]  [bit_val] The intended value of that bit - {0,1}
 *
 * \todo Don't have the function exit, but perhaps print a warning or at worst throw an exception
 */
void SetWordBit(unsigned int& word, int bit_location, int bit_val){
  if(bit_val==0){
      // n=3
      // prev      = 01101011 
      // mask      = 11110111 [~(1<<3)]
      // prev&mask = 01100011 [only bit 3 has changed]
      word &= ~( 1 << bit_location );
  }
  else if(bit_val==1){
      // n=4
      // prev      = 01101011 
      // mask      = 00010000 [(1<<4)]
      // prev|mask = 01111011 [only bit 4 has changed]
      word |=  ( 1 << bit_location );
  }
  else{
      INFO("This is not a valid bit value to set - exitting");
      exit(18);
  }

}

/*!
 * \brief Decode bit of word
 *
 * Decode the value of a particular bit or a single word
 *
 * \param[in]  [word] The word that is to be decoded
 * \param[in]  [bit_location] The bit location that you want to manipulate
 *
 */
int GetWordBit(unsigned int word, int bit_location){
  // obtain the value of a particular bit in a word

  word =  (word>>bit_location);
  word &= 0x1;
  
  return word;
}

/*!
 * \brief Get current date
 *
 * Gets the date, in the format of <DateYYYYMMDD_TimeHHMMSS> and returns this as a string.  This can be useful if you want to put a timestamp on someting, like an output text file.
 *
 * \return A string which is the current time, formatted as <DateYYYYMMDD_TimeHHMMSS>
 *
 */
std::string GetDateVerboseString(){

  std::string verboseDate;
  
  using namespace std;
  using namespace std::chrono;
  typedef duration<int, ratio_multiply<hours::period, ratio<24> >::type> days;
  system_clock::time_point now = system_clock::now();
  system_clock::duration tp = now.time_since_epoch();
  days d = duration_cast<days>(tp);
  tp -= d;
  hours h = duration_cast<hours>(tp);
  tp -= h;
  minutes m = duration_cast<minutes>(tp);
  tp -= m;
  seconds s = duration_cast<seconds>(tp);
  tp -= s;
  INFO(d.count() << "d " << h.count() << ':' << m.count() << ':' << s.count());
  INFO(" " << tp.count() << "[" << system_clock::duration::period::num << '/' << system_clock::duration::period::den << "]");

  time_t tt = system_clock::to_time_t(now);
  tm utc_tm = *gmtime(&tt);
  tm local_tm = *localtime(&tt);
  INFO(utc_tm.tm_year+1900);
  INFO(utc_tm.tm_mon+1);
  INFO(utc_tm.tm_mday);
  INFO(utc_tm.tm_hour);
  INFO(utc_tm.tm_min);
  INFO(utc_tm.tm_sec);
  
  //desciptor
  verboseDate += "Date";
  
  // year
  verboseDate += std::to_string( utc_tm.tm_year+1900 );
  
  // month
  if(utc_tm.tm_mon+1 < 10){
    verboseDate += "0";
    verboseDate += std::to_string( utc_tm.tm_mon+1 );
  }
  else
    verboseDate += std::to_string( utc_tm.tm_mon+1 );
    
  // day
  if(utc_tm.tm_mon+1 < 10){
    verboseDate += "0";
    verboseDate += std::to_string( utc_tm.tm_mday );
  }
  else{
    verboseDate += std::to_string( utc_tm.tm_mday );
  }
  
  //desciptor
  verboseDate += "_Time";
  
  // hour
  if(utc_tm.tm_hour < 10){
    verboseDate += "0";
    verboseDate += std::to_string( utc_tm.tm_hour );
  }
  else{
    verboseDate += std::to_string( utc_tm.tm_hour );
  }

  // minute
  if(utc_tm.tm_min < 10){
    verboseDate += "0";
    verboseDate += std::to_string( utc_tm.tm_min );
  }
  else{
    verboseDate += std::to_string( utc_tm.tm_min );
  }

  // second
  if(utc_tm.tm_sec < 10){
    verboseDate += "0";
    verboseDate += std::to_string( utc_tm.tm_sec );
  }
  else{
    verboseDate += std::to_string( utc_tm.tm_sec );
  }


  return verboseDate;
}

/*!
 * \brief Pause running of process
 *
 * Pause the running of the current process for an amount of time specified in [ns]
 *
 * \param[in] [nseconds] Duration of pause in [ns]
 *
 */
void Wait(float nseconds){
    DEBUG("Waiting for : "<<nseconds<<" seconds");
    int nmicroseconds = floor(nseconds*1000000.0);
    usleep(nmicroseconds);
}

/*!
 * \brief Convert hexidecimal to string
 *
 * Converts a hexidecimal number to a string
 *
 * \param[in] [input] Input hexidecimal number to be converted
 * \return The string representation of the hexidecimal value
 * 
 */
std::string ConvertDecToHexString(int input){
  std::stringstream ss;
  ss << std::hex << input;
  std::string res ( ss.str() );
  return res;
}

/*!
 * \brief Is the character a digit
 *
 * Determine if the character passed is a digit
 *
 * \param[in] [input] Input character to verify
 * \return Boolean determination of whether the character is a number
 * \todo Surely there is a more standard C++ way of doing this, no?
 */
bool IsNumber(char input){
  bool val=false;

  if(input=='0') val=true;
  if(input=='1') val=true;
  if(input=='2') val=true;
  if(input=='3') val=true;
  if(input=='4') val=true;
  if(input=='5') val=true;
  if(input=='6') val=true;
  if(input=='7') val=true;
  if(input=='8') val=true;
  if(input=='9') val=true;

  return val;
}

/*!
 * \brief Is the character a period
 *
 * Determine if the character passed is a period
 *
 * \param[in] [input] Input character to verify
 * \return Boolean determination of whether the character is a period
 * \todo Surely there is a more standard C++ way of doing this, no? This is really silly to implement like this.  At least Sam things so, and he implemented it.
 */
bool IsDot(char input){
  bool val=false;

  if(input=='.') val=true;

  return val;
}


/*!
 * \brief Is the string an IP address
 *
 * Determine if the string takes the appropriate format of an IP address.  In FASER, this is used at configuration so that you need not always provide the cryptic IP address assigned by DHCP to the interface board, but rather the verbose human identifiable name(e.g. faser-vme-controller00)
 *
 * \param[in] [input] Input string to verify
 * \return Boolean determination of whether the string is an IP address
 *
 */
bool IsIPAddress(const char *input){

  // current character to check next character
  // starts at -1 and then '.'=0 and '<#>'=1
  int prev=-1;

  // counting the number of dots encountered
  int ndot=0;

  // the count for an sequence of three numbers
  int nnum=0;

  // count the number of dots and register that only numbers exist in between
  // there should be three dots with numbers before and after
  int length = (unsigned)strlen(input);
  DEBUG("The length of the IP Address is : "<<length);

  for(int i=0; i<length; i++){

    if(prev==-1){
      // if the first character check that its a number
      if(IsNumber(input[i])){
        prev=1;
      }
      else{
        INFO("IP address must begin with number ...");
        return false;
      }
    }
    else if(prev==0){
      // if the previous character was a dot, then anticipate a number
      if(IsNumber(input[i])){
        prev=1;
        nnum++;
      }
      else if(IsDot(input[i])){
        INFO("IP address can't have two dots in a row");
        return false;
      }
      else{
        INFO("IP address is not an allowed IP address character");
        return false;
      }

    }
    else if(prev==1){
      // if the previous character was a number, then anticipate a number
      if(IsNumber(input[i])){
        prev=1;
        if(nnum<3){ // can't have more than three numbers in a row between dots
          nnum++;
        }
        else{
          INFO("IP address has too many numbers in a row");
          return false;
        }
      }
      else if(IsDot(input[i])){
        // if you find a number after a dot, its alright, just start over counting
        prev=0;
        nnum=0;
        if(ndot<3){
          ndot++;
        }
        else{
          INFO("IP address has too many dots");
          return false;
        }
      }
      else{
        INFO("IP address is not an allowed this character");
        return false;
      }
    }
  }

  if(ndot<3){
    INFO("Not enough dots to be an IP address");
    return false;
  }

  // if it makes it through without failing, it must be an IP address format
  return true;
}

// gets the IP address given either a hardcoded IP address of the desired hostname
/*!
 * \brief Find IP address
 *
 * Searches for the IP address of the given computer name.  In FASER, this is used at configuration so that you need not always provide the cryptic IP address assigned by DHCP to the interface board, but rather the verbose human identifiable name(e.g. faser-vme-controller00)
 *
 * \param[in] [input] The input compute name to search for.
 * \return The string which is the IP address found
 * \todo Make error handling more robust in the case that there is a bug in the name and no IP address is found
 */
std::string GetIPAddress(const char* input){

  char ip_addr_string[32] ;

  // check structure of the input address
  if(IsIPAddress(input)){
    // if it is registered as a hostname word, then convert to IP
    INFO("Interpreting as direct IP address");

    // if it is a numerical IP address then save it
    strcpy(ip_addr_string, std::string(input).c_str() ) ; // SIS3153 IP address
  }
  else{
    // if it is registered as a hostname word, then convert to IP
    INFO("Interpreting as hostname and translating to IP");

    struct hostent *hp = gethostbyname(input);

    if (hp == NULL) {
      ERROR("gethostbyname() failed\n");
    }
    else {
      DEBUG("Getting IP from hostname : "<<hp->h_name);
      unsigned int i=0;
      while ( hp -> h_addr_list[i] != NULL) {
        strcpy(ip_addr_string, inet_ntoa( *( struct in_addr*)( hp -> h_addr_list[i])));
        INFO( "Found IP address : "<< inet_ntoa( *( struct in_addr*)( hp -> h_addr_list[i])));
        i++;
      }
      printf("\n");
    }

  }

  INFO("Discovered digitizer IP address : "<<ip_addr_string);

  std::string ip_str = std::string(ip_addr_string);
  
  INFO("Discovered digitizer IP address string : "<<ip_str);

  return ip_str;
}
