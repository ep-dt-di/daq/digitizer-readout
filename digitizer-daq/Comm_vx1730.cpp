/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/

/*!
 * \file Comm_vx1730.cpp
 * \copyright Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
 * \author Sam Meehan
 * \date 13 Jan 2021
 *
 * \brief Main communication interface for the digitizer.
 *
 * \details This provides the main class and set of methods to control the digitizer setup.  If you have a functioning physical setup, then the vx1730 class is the thing that you must instantiate.  There is one instance per board.
 */

#include "Comm_vx1730.h"

/*!
 * \brief Constructor
 *
 * The constructor of the object that facilitates communication with the digitizer board
 *
 * \param[in] [ip] The ethernet IP address of the interface board of the VME crate in which your digitizer resides.
 * \param[in] [vme_base] The base address of the digitizer board, set by the physical rotary switches on the digitizer.  See the CAEN documentation for how to set this address.
 */
vx1730::vx1730(char ip[], unsigned int vme_base){
  INFO("vx1730::Constructor");
      
  // open the vme crate connection
  // the vme_crate is a member of this class 
  // and we are connecting it with an IP address here
  sis3153eth(&m_crate, ip);  
  
  int return_code;
  char char_messages[128] ;
  unsigned int nof_found_devices ;

  // open Vme interface
  return_code = m_crate->vmeopen();  
  
  // open Vme interface
  m_crate->get_vmeopen_messages(char_messages, &nof_found_devices);  

  
  INFO("Openning VME connection : ");
  INFO("get_vmeopen_messages = "<<setw(20)<<char_messages);
  INFO("nof_found_devices    = "<<setw(20)<<nof_found_devices);
    
  // set the base address of the single vx1730 card itself
  // would need to extend this in the case of multiple cards
  m_base_address = vme_base;
    
}

/*!
 * \brief Destructor
 *
 * Cleans up the use of the class, namely the pointer to the m_crate object which is necessarily
 * instantiated to provide a bridge through the interface board.
 *
 */
vx1730::~vx1730(){
  INFO("");
  delete m_crate;
}

/*!
 * \brief Test sis3153 communication
 *
 * Tests the read/write capabilities of the sis3153 interface board.
 *
 * \param[in] config The full json config file object of settings necessary to configure the vx1730 digitizer.
 *
 * \callgraph
 * \callergraph
 * \showrefby
 */
void vx1730::TestCommInterface(json config){
  sis3153_TestComm( m_crate, true );
} 

/*!
 * \brief Test vx1730 communication
 *
 * Tests the read/write capabilities of the vx1730 digitizer board. 
 *
 * \param[in] [config] The full json config file object of settings necessary to configure the vx1730 digitizer.
 * \note It only makes sense to use this after having assured that the ethernet communication with the interface board is robust.
 * \callgraph
 * \callergraph
 * \showrefby
 * \todo Enter the hardcoded validation information for the EHN1 board specs
 */
void vx1730::TestCommDigitizer(json config){
  // test that the communication channels 

  int return_code;
  unsigned int data;
  unsigned int addr;

  INFO("Testing read/write to scratch space on digitizer");

  // testing scratch space
  ReadSlaveReg(m_crate, m_base_address+VX1730_SCRATCH, data );
  WriteSlaveReg(m_crate, m_base_address+VX1730_SCRATCH, 0x22222222 );
  ReadSlaveReg(m_crate, m_base_address+VX1730_SCRATCH, data );
  WriteSlaveReg(m_crate, m_base_address+VX1730_SCRATCH, 0x10101010 );
  ReadSlaveReg(m_crate, m_base_address+VX1730_SCRATCH, data );
  
  // testing scratch space on each channel
  ReadSlaveReg(m_crate, m_base_address+VX1730_CHANNEL_DUMMY32, data );
  WriteSlaveReg(m_crate, m_base_address+VX1730_CHANNEL_DUMMY32, 0x55555555 );
  ReadSlaveReg(m_crate, m_base_address+VX1730_CHANNEL_DUMMY32, data );
  WriteSlaveReg(m_crate, m_base_address+VX1730_CHANNEL_DUMMY32, 0x30303030 );
  ReadSlaveReg(m_crate, m_base_address+VX1730_CHANNEL_DUMMY32, data );

  INFO("Check Fixed Configurations");
  // three registers that should be fixed
  // ToDo: check that they match manufacturer specs
  
  unsigned int data_VX1730_CONFIG_ROM;
  unsigned int data_VX1730_CONFIG_ROM_BOARD_VERSION;
  unsigned int data_VX1730_CONFIG_ROM_BOARD_FORMFACTOR;
  unsigned int data_VX1730_ROC_FPGA_FW_REV;

  ReadSlaveReg(m_crate, m_base_address+VX1730_CONFIG_ROM, data_VX1730_CONFIG_ROM );
  ReadSlaveReg(m_crate, m_base_address+VX1730_CONFIG_ROM_BOARD_VERSION, data_VX1730_CONFIG_ROM_BOARD_VERSION );
  ReadSlaveReg(m_crate, m_base_address+VX1730_CONFIG_ROM_BOARD_FORMFACTOR, data_VX1730_CONFIG_ROM_BOARD_FORMFACTOR );
  ReadSlaveReg(m_crate, m_base_address+VX1730_ROC_FPGA_FW_REV, data_VX1730_ROC_FPGA_FW_REV );

  INFO("VX1730_CONFIG_ROM : "<<data_VX1730_CONFIG_ROM );
  INFO("VX1730_CONFIG_ROM_BOARD_VERSION : "<<data_VX1730_CONFIG_ROM_BOARD_VERSION );
  INFO("VX1730_CONFIG_ROM_BOARD_FORMFACTOR : "<<data_VX1730_CONFIG_ROM_BOARD_FORMFACTOR );
  INFO("VX1730_ROC_FPGA_FW_REV : "<<data_VX1730_ROC_FPGA_FW_REV );

  INFO("BoardType config : "<<config["board_type"]);
  if(config["board_type"]=="v0"){
    INFO("Validating board type : v0");
    if(data_VX1730_CONFIG_ROM != 0x6)
      WARNING("VX1730_CONFIG_ROM : This is not consistent with board v0 specs.");
    if(data_VX1730_CONFIG_ROM_BOARD_VERSION != 0xc0)
      WARNING("VX1730_CONFIG_ROM_BOARD_VERSION : This is not consistent with board v0 specs.");
    if(data_VX1730_CONFIG_ROM_BOARD_FORMFACTOR != 0x1)
      WARNING("VX1730_CONFIG_ROM_BOARD_FORMFACTOR : This is not consistent with board v0 specs.");
    if(data_VX1730_ROC_FPGA_FW_REV != 0x17200410)
      WARNING("VX1730_ROC_FPGA_FW_REV : This is not consistent with board v0 specs.");
  }
  else if(config["board_type"]=="v1"){
    INFO("Validating board type : v1");
    WARNING("No info yet coded for board type v1 - please enter this for the new digitizer soon");
  }
  else{
    WARNING("This is not a valid board type to verify.");
  }

  INFO("Check firmware on each digitizer channel");
  for(unsigned int iChan=0; iChan<NCHANNELS; iChan++){
    ReadSlaveReg(m_crate, m_base_address+VX1730_CHANNEL_AMC_FIRMWARE_VERSION+(0x0100*iChan), data );
    if(config["board_type"]=="v0"){
      if(data!=0x19010007)
        WARNING("VX1730_CHANNEL_AMC_FIRMWARE_VERSION : This is not consistent with board v0 specs for channel : "<<iChan);
    }
    if(config["board_type"]=="v1"){
      WARNING("VX1730_CHANNEL_AMC_FIRMWARE_VERSION : No info entered for new digitizer for board type v1");
    }
  }
}

/*!
 * \brief Use jumbo ethernet packets
 *
 * Changes the mode of communication for the ethernet with the sis3153 interface board. If toggled to be true, then it will use [jumbo frames](https://en.wikipedia.org/wiki/Jumbo_frame) which allow for larger packets of information to be sent in one ethernet transfer.
 *
 * \param[in] [toggle] A single boolean which turns the jumbo packets to on (true) or off (false).
 * \sa [What the heck is a jumbo frame?](https://en.wikipedia.org/wiki/Jumbo_frame)
 * \warning If you are going to use jumbo frames then you must remember to configure your ethernet network appropriately, or it will not function appropriately.
 * \callgraph
 * \callergraph
 * \showrefby
 */
void vx1730::SetInterfaceEthernetJumboFrames(bool toggle){
  // toggle jumbo frames on/off for the ethernet communication to the interface board
  bool set_interface_jumbo = toggle;
  
  INFO("Interface Jumbo : "<<set_interface_jumbo);
  
  INFO("Ethernet Jumbo Frame state : ");
  m_crate->get_UdpSocketJumboFrameStatus();
  
  if(m_crate->get_UdpSocketJumboFrameStatus()){
    INFO("JumboFrames : enabled");
  }
  else{
    INFO("JumboFrames : disabled");
  }
  
  if(set_interface_jumbo){
    INFO("Turning on Ethernet Jumbo Frame state");
    m_crate->set_UdpSocketEnableJumboFrame();
  }
  
  if(m_crate->get_UdpSocketJumboFrameStatus()){
    INFO("JumboFrames : enabled");
  }
  else{
    INFO("JumboFrames : disabled");
  }

}

/*!
 * \brief Configure the ethernet transmission
 *
 * Configures the maximum number of ethernet packets that the interface board can attempt to send in a single transmission.
 *
 * \param[in] [n_max_packets] [The maximum number of ethernet packets that the interface board can attempt to send in one transmission.]
 * \note It is recommended and most stable to use a value of 1, which is the default.
 * \warning Be careful using anything over a value of 2, as this was found to corrupt data.
 * \callgraph
 * \callergraph
 * \showrefby
 */
void vx1730::SetInterfaceEthernetMaxPackets(int n_max_packets){
  // change the maximum number of packets to be sent over a single ethernet packet

  unsigned int set_interface_max_packets = n_max_packets;
  INFO("Interface Max Packets : "<<set_interface_max_packets);
  
  //int udpPackets = m_crate->get_UdpSocketReceiveNofPackagesPerRequest();
  //INFO("UDPPackets(1) : "<<std::dec<<udpPackets);
  
  int udpWords = m_crate->get_UdpSocketNofReadMaxLWordsPerRequest();
  INFO("UDPWords(1) : "<<std::dec<<udpWords);
  
  m_crate->set_UdpSocketReceiveNofPackagesPerRequest(set_interface_max_packets);
  
  //udpPackets = m_crate->get_UdpSocketReceiveNofPackagesPerRequest();
  //INFO("UDPPackets(2) : "<<std::dec<<udpPackets);
  
  udpWords = m_crate->get_UdpSocketNofReadMaxLWordsPerRequest();
  INFO("UDPWords(2) : "<<std::dec<<udpWords);

}

/*!
 * \brief Configure time between ethernet transmissions
 *
 * Configures the time that the interface pauses between ethernet transmissions for the sis3153 board.
 *
 * \param[in] [gap_setting] [The value, corresponding to an amount of time specified in the [sis3153 board documentation]()]
 * \sa [Page]
 * \note It is typical to keep this at the default value so as to not cause any undue delay and decrease in data rate transmission.
 * \callgraph
 * \callergraph
 * \showrefby
 */
void vx1730::SetInterfaceEthernetGap(int gap_setting){
  unsigned int set_interface_packet_gap = gap_setting;
  INFO("Interface Packet Gap : "<<set_interface_packet_gap);

  int udpGap = m_crate->get_UdpSocketGapValue();
  INFO("UDPGap : "<<udpGap);
  
  int udpGapStatus = m_crate->set_UdpSocketGapValue(set_interface_packet_gap);
  INFO("UDPGapStatus : "<<udpGapStatus);
  
  udpGap = m_crate->get_UdpSocketGapValue();
  INFO("UDPGap : "<<udpGap);
}

/*!
 * \brief Ethernet speed test
 *
 * Perform a reading test of the sis3153 board by continually reading large blocks of data from the test read space and measuring the amount of time for each read.  This is done multiple times and the average is taken as the recorded speed of the communication.  The returned value is in terms of 32-bit words per second.  
 *
 * \param[in] [nreads] [The number of times you want to perform the test]
 * \return [The output data transfer rate in words/second]
 * \warning If you are pressed for time, then be careful, this can take a while to do (relatively speaking).
 * \callgraph
 * \callergraph
 * \showrefby
 */
float vx1730::PerformInterfaceSpeedTest(int nreads){
  INFO("Performing interface card speed test for ethernet");
  float total_n_words=0;
  
  auto start = chrono::high_resolution_clock::now(); 

  // perform a fixed numer of reads of test space and measure the time
  uint32_t* lots_of_data=new uint32_t[0x100000];
  for(int iread=0; iread<nreads; iread++){  
    // speed test
    uint32_t n_lots_of_words;
    int status_speed = m_crate->udp_sis3153_register_dma_read (0x002, lots_of_data, 0xFFFFF, &n_lots_of_words );

    DEBUG("SIS3153Ethernet Speed : "<<n_lots_of_words);
    
    total_n_words += n_lots_of_words;
  }

  auto end = chrono::high_resolution_clock::now(); 
  delete [] lots_of_data;
  float time_taken = chrono::duration_cast<chrono::nanoseconds>(end - start).count() * 1e-9; 
  float rate = total_n_words/time_taken;
  INFO("Speed test result : nreads=" << nreads << "  total_n_words=" << total_n_words << "  time_taken=" << time_taken << " rate=" << rate << setprecision(5) << " sec ");

  return rate;
}

/*!
 * \brief Ethernet+VME speed test
 *
 * Perform a reading test of the vx1730 board (which ultimately measures the data transfer rate we care about - VME+Interface+Ethernet) by continually reading large blocks of data from the test read space and measuring the amount of time for each read.  This is done multiple times and the average is taken as the recorded speed of the communication.  The returned value is in terms of 32-bit words per second.  
 *
 * \param[in] [nreads] [The number of times you want to perform the test]
 * \return [The output data transfer rate in words/second]
 * \note If you are pressed for time, then be careful, this can take a while to do (relatively speaking).
 * \warning This causes issues if run when there is no data and will put the board into a corrupt state.
 * \callgraph
 * \callergraph
 * \showrefby
 * \todo Find a way to not corrupt the board when running with no data.  Maybe configure/self-trigger the board a few times before running this?
 */
float vx1730::PerformInterfaceVMESpeedTest(int nreads){
  INFO("Performing VME+interface card speed test for ethernet");

  float total_n_words=0;
  
  auto start = chrono::high_resolution_clock::now(); 

  for(int iread=0; iread<nreads; iread++){
    // speed test
    uint32_t lots_of_data[0x100000];
    uint32_t n_lots_of_words;

    int status_speed = m_crate->vme_A32_2ESST320FIFO_read (m_base_address+0x0, lots_of_data, 0x10000, &n_lots_of_words);

    DEBUG("vx1730Ethernet Speed : "<<std::dec<<n_lots_of_words);
    
    total_n_words += n_lots_of_words;
  }
  
  auto end = chrono::high_resolution_clock::now(); 

  float time_taken = chrono::duration_cast<chrono::nanoseconds>(end - start).count() * 1e-9; 
  float rate = total_n_words/time_taken;
  INFO("Speed test result : nreads=" << nreads << "  total_n_words=" << total_n_words << "  time_taken=" << time_taken << " rate=" << rate << setprecision(5) << " sec ");

  return rate;

}

/*!
 * \brief Full configuration of digitizer
 *
 * Performs a sequence of individual configuration procedures, the details of which are controlled by a top level json object which contains the settings desired for running (e.g. thresholds).  After this configuration, the board is in a state ready to run, but the actual acquisition will not begin until the StartAcquisition() method is called.
 *
 * \param[in] [config] [The json configuration object that contains the desired settings for data taking.]
 * \param[in] [debug]  [A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.
 * \param[out] [name of output parameter] [its description]
 * \callgraph
 * \callergraph
 * \showrefby
 * \todo Find a better way to pass the debug flag through these methods since the daq system has logging methods.  This method is rather rudimentary it seems.
 */
void vx1730::Configure( json config, bool debug){  
  
  INFO("==================================");
  INFO("INITIAL state before configuration");
  INFO("==================================");
  DumpConfig();
  
  ConfigReset(config, debug);
  
  ConfigBuffer(config, debug);
  
  ConfigReadoutEnable(config, debug);
  
  ConfigVoltageReading(config, debug);
  
  ConfigBlockReadout(config, debug);
  
  ConfigEventReadoutTrigger(config, debug);
  
  ConfigGroupTriggerSettings(config, debug);

  ConfigLVDS(config, debug);
  
  INFO("==================================");
  INFO("FINAL state after configuration");
  INFO("==================================");
  DumpConfig();

}

/*!
 * \brief Board reset
 *
 * Performs a complete reset of the digitizer board to its default state.
 *
 * \param[in] [config] [The json configuration object that contains the desired settings for data taking.]
 * \param[in] [debug]  [A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.
 * \callgraph
 * \callergraph
 * \showrefby
 */
void vx1730::ConfigReset( json config, bool debug){
  // reseting the SW will screw with the ability to write a DC offset 
  INFO("Reset system configurations to default");
  WriteSlaveReg(m_crate, m_base_address+VX1730_SW_RESET, 0x1111, debug );

  unsigned int reset_data;

  // wait to proceed until ALL channels are not busy
  bool reset_busy=true;
  while(reset_busy){
    // assume that the busy is finished and set it to be true if you find any one channel to be busy
    reset_busy=false;
  
    // check each channel to see if it is busy while waiting for reset
    for(int iChan=0; iChan<16; iChan++){
      ReadSlaveReg(m_crate, m_base_address+VX1730_CHANNEL_STATUS+(0x0100)*iChan, reset_data, true );
      DEBUG("Check channel status : "<<iChan<<" - "<<ConvertIntToWord(reset_data));
      
      // if any channel is busy
      if(GetWordBit(reset_data,2)){
        reset_busy=true;
      }
    }
    
    // the wait is necessary because it takes some time for the reset to propogate
    Wait(0.1);
  }
  INFO("Successful system configuration reset");

  // clear the memory buffer of any leftover events
  INFO("Clearing memory buffers");
  WriteSlaveReg(m_crate, m_base_address+VX1730_SW_CLEAR, 0x1111, debug );
}

/*!
 * \brief Digitizer hardware buffer setting
 *
 * Configures the size of the hardware buffer (i.e. the acquisition window) that will be recorded upon a single acquisition trigger.  It will also configure the number of "post-trigger" samples which ultimately affects the delay of the signal within the acquired window.
 *
 * \param[in] [config] [The json configuration object that contains the desired settings for data taking.]
 * \param[in] [debug]  [A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.
 * \callgraph
 * \callergraph
 * \showrefby
 */
void vx1730::ConfigBuffer( json config, bool debug){
  INFO("Configuring memory buffer");

  // will it be a custom buffer length
  bool custom = (bool)config["buffer"]["custom"];

  // buffer length transforms to the number of buffers before writing to the register
  unsigned int buffer_length = (unsigned int)config["buffer"]["length"];
  INFO("Input configuration for rolling buffer length : "<<buffer_length);

  // by default, the length will be set to 0xA
  // custom : 0xA --> NLoc = 10 --> BufferLength = 10*10
  // fixed  : 0xA --> BufferLength = 630
  unsigned int n_buffer = 0x0A;

  if(custom==true){
    if(buffer_length>639990){
      ERROR("This buffer is too long for the memory, exitting gracefully ...");
      THROW(DigitizerHardwareException, "This custom buffer will be too large");
    }
  
    n_buffer = buffer_length/10;
    
    DEBUG("Custom Length Buffer : Setting n_buffer to 0x"<<std::hex<<n_buffer<<" which corresponds to a length of "<<std::dec<<buffer_length);
    WriteSlaveReg(m_crate, m_base_address+VX1730_CUSTOM_SIZE, n_buffer, debug );
  }
  else if(custom==false){
    if(buffer_length==630)
      n_buffer = 0x0A;
    else if(buffer_length==1240)
      n_buffer = 0x09;
    else if(buffer_length==2490)
      n_buffer = 0x08;
    else if(buffer_length==4990)
      n_buffer = 0x07;
    else if(buffer_length==9990)
      n_buffer = 0x06;
    else if(buffer_length==19990)
      n_buffer = 0x05;
    else if(buffer_length==39990)
      n_buffer = 0x04;
    else if(buffer_length==79990)
      n_buffer = 0x03;
    else if(buffer_length==159990)
      n_buffer = 0x02;
    else if(buffer_length==319990)
      n_buffer = 0x01;
    else if(buffer_length==639990)
      n_buffer = 0x00;
    else{
      WARNING("This is not a possible Buffer Length configuration - setting to default of 630 samples.");
      n_buffer = 0x0A;
    }
    
    DEBUG("Fixed Length Buffer : Setting n_buffer to 0x"<<std::hex<<n_buffer<<" which corresponds to a length of "<<std::dec<<buffer_length);
    WriteSlaveReg(m_crate, m_base_address+VX1730_BUFFER_ORGANIZATION, n_buffer, debug );
    WriteSlaveReg(m_crate, m_base_address+VX1730_CUSTOM_SIZE, 0x0, debug ); // to ensure the value of custom length is 0
  }
  else{
    ERROR("Something is very strange with the buffer length configuration");
  }
  
  
  // number of post samples is configurable but must be divisible by four because of the formula for this board
  // so we first transform to the closest value divisible by four which then gets put into 
  //
  // Npost = PostTriggerValue*N + ConstantLatency
  // Npost = number of post trigger samples.
  // PostTriggerValue = content of this register.
  // N = coefficient to be multiplied by the PostTriggerValue (N = 4 for 730 and N = 2 for 725).
  // ConstantLatency = constant number of samples added due to the latency associated to the trigger 
  //                   processing logic in the ROC FPGA. The value of this constant depends on the trigger source used 
  //                   and can change between different firmware revisions.
  
  unsigned int PostTriggerValue = 0x20;
  unsigned int NPostMin = (unsigned int)config["buffer"]["n_post_samples"];
  INFO("Input configuration for N(postsample) : "<<NPostMin);
  
  if(NPostMin%4!=0){
    WARNING("Your choice of N(postsample) is not valid [must be divisible by 4 due to vx1730 constraints]");
    WARNING("Setting to be the next highest number of post samples : "<<std::ceil(NPostMin/4.0)*4.0);
  }
  
  // convert to the proper input to the register according to the formulas above
  PostTriggerValue = (unsigned int)std::ceil(NPostMin/4.0);
  
  INFO("Setting PostTriggerValue to 0x"<<std::hex<<PostTriggerValue);
  INFO("This corresponds to "<<std::dec<<(int)PostTriggerValue*4.0<<" plus the constant latency of the board (~50 samples).");
  WriteSlaveReg(m_crate, m_base_address+VX1730_POST_TRIGGER_SETTING, PostTriggerValue, debug );
}

/*!
 * \brief Configures readout channels
 *
 * Configures which of the 16 readout channels will record data upon receiving an acquisition trigger.
 *
 * \param[in] [config] [The json configuration object that contains the desired settings for data taking.]
 * \param[in] [debug]  [A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.
 * \callgraph
 * \callergraph
 * \showrefby
 */
void vx1730::ConfigReadoutEnable( json config, bool debug){
  INFO("Configuring readout channels");
  
  unsigned int data_enable_mask;
  ReadSlaveReg(m_crate, m_base_address + VX1730_CHANNEL_EN_MASK, data_enable_mask, debug );
  DEBUG("Current Readout Enable Mask : "<< ConvertIntToWord(data_enable_mask));
  
  // for ensuring that you configure the readout for every channel only once
  bool readout_set[16] = {false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false};
  
  for(int iChan=0; iChan<config["channel_readout"].size(); iChan++){  
    DEBUG("Configuring input : "<<iChan);

    int channel = (int)config["channel_readout"].at(iChan)["channel"];
    int enable  = (int)config["channel_readout"].at(iChan)["enable"];
    
    if(readout_set[channel]==false){
      readout_set[channel]=true;
    }
    else{
      WARNING("You are trying to set the readout config twice for channel : "<<channel);
    }
    
    SetWordBit(data_enable_mask, channel, enable);
  }
  
  // check that all channels have been configured
  for(int iChan=0; iChan<16; iChan++){
    if(readout_set[iChan]==false)
      WARNING("You did not set the readout bit for channel : "<<iChan);
  }
  
  // writing enable mask
  WriteSlaveReg(m_crate, m_base_address + VX1730_CHANNEL_EN_MASK, data_enable_mask, debug );
  ReadSlaveReg(m_crate, m_base_address + VX1730_CHANNEL_EN_MASK, data_enable_mask, debug );
  DEBUG("Final Readout Enable Mask : "<< ConvertIntToWord(data_enable_mask));
}

/*!
 * \brief Configure channel reading dynamics
 *
 * For each of the 16 channels, configures the dynamic range and DC offset specified in the configuration file.
 *
 * \param[in] [config] [The json configuration object that contains the desired settings for data taking.]
 * \param[in] [debug]  [A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.
 * \callgraph
 * \callergraph
 * \showrefby
 */
void vx1730::ConfigVoltageReading( json config, bool debug){
  INFO("Configuring channel voltage readings [Dynamic Range, DC Offsets]");
  
  bool readout_set[16] = {false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false};

  for(int iChan=0; iChan<config["channel_readout"].size(); iChan++){
  
    unsigned int channel = config["channel_readout"].at(iChan)["channel"];
    
    if(readout_set[channel]==false){
      readout_set[channel]=true;
    }
    else{
      WARNING("You are trying to set the readout config twice for channel : "<<channel);
    }
    
    // get info from config
    double dynamic_range = (double)config["channel_readout"].at(iChan)["dynamic_range"];
    double dc_offset     = (double)config["channel_readout"].at(iChan)["dc_offset"];  
    
    DEBUG("Channel : "<<channel<<" - DynamicRange = "<<dynamic_range<<"  DCOffset = "<<dc_offset);
  
    // dynamic range
    SetChannelDynamicRange(channel, dynamic_range);
    
    // dc offset
    SetChannelDCOffset(channel, dc_offset);     
  }
  
  // check that all channels have been configured
  for(int iChan=0; iChan<16; iChan++){
    if(readout_set[iChan]==false)
      WARNING("You did not set the readout bit for channel : "<<iChan);
  }
}

/*!
 * \brief Configure VME transmission protocol
 *
 * Configures the parameters for a VME block read.  
 *
 * \param[in] [config] [The json configuration object that contains the desired settings for data taking.]
 * \param[in] [debug]  [A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.
 * \callgraph
 * \callergraph
 * \showrefby
 */
void vx1730::ConfigBlockReadout( json config, bool debug){
  INFO("Configuring BLT block event readout");

  // ToDo : Make this configurable for setting the number of events to be read out in a single block transfer
  unsigned int readout_blt = (int)config["readout"]["readout_blt"];
  if(readout_blt>1024){
    WARNING("Maximum value for number of events in the buffer is 1024");
    WARNING("Setting BLT readout value to 1024");
    readout_blt = 1024;
  }

  unsigned int readout_blt_check;
  INFO("Number of BLT events to read : "<<readout_blt);
  WriteSlaveReg(m_crate, m_base_address + VX1730_BLT_EVENT_NB, readout_blt, debug );
  ReadSlaveReg(m_crate, m_base_address + VX1730_BLT_EVENT_NB, readout_blt_check, debug );
  
  // enable BERR to stop block transfer readout
  unsigned int data_readout_control;
  ReadSlaveReg(m_crate, m_base_address + VX1730_VME_CONTROL, data_readout_control, debug );
  DEBUG("Readout Control : "<<ConvertIntToWord(data_readout_control));
  SetWordBit(data_readout_control, 4, 1);
  
  WriteSlaveReg(m_crate, m_base_address + VX1730_VME_CONTROL, data_readout_control, debug );
  ReadSlaveReg(m_crate, m_base_address + VX1730_VME_CONTROL, data_readout_control, debug );
  DEBUG("Readout Control : "<<ConvertIntToWord(data_readout_control));


  // check the software buffer size vs possible event size and modify BLT size if necessary
  int sw_buffer         = (int)config["readout"]["software_buffer"];
  int buffer_length     = (int)config["buffer"]["length"];
  int nchannels_enabled = 0;
  for(int ich=0; ich<(int)config["channel_readout"].size(); ich++){
    if((int)config["channel_readout"].at(ich)["enable"]==1){
      nchannels_enabled++;
    }
  }

  int event_size = ((int)(buffer_length/2) * nchannels_enabled) + 4;

  if(config["readout"]["readout_method"]!="BLT32"){
    if(event_size*readout_blt > sw_buffer){
      WARNING("You may be in dangerous territory with this configuration if the event rate gets too high");
      WARNING("SW Buffer Size [words] : "<<sw_buffer);
      WARNING("Single Event Size [words] : "<<event_size);
      WARNING("BLT Read Size [NEvents] : "<<readout_blt);
      WARNING("I am going to modify the BLT setting to ensure this does not happen");

      int new_readout_blt = (int)(sw_buffer/event_size);

      WARNING("I am resetting the BLT value to : "<<new_readout_blt);
      WriteSlaveReg(m_crate, m_base_address + VX1730_BLT_EVENT_NB, new_readout_blt, debug );
      ReadSlaveReg(m_crate, m_base_address + VX1730_BLT_EVENT_NB, readout_blt_check, debug );
      INFO("Check new number of BLT events to read : "<<readout_blt);

    }
  }
  
}

/*!
 * \brief Configure trigger type
 *
 * Configures what signals are responsible for an acquisition trigger.
 *
 * \param[in] [config] [The json configuration object that contains the desired settings for data taking.]
 * \param[in] [debug]  [A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.
 * \callgraph
 * \callergraph
 * \showrefby
 */
void vx1730::ConfigEventReadoutTrigger( json config, bool debug){
  INFO("Configuring acquisition trigger");
  
  DEBUG("Acquiring global trigger settings");
  unsigned int global_lvds     = 0;
  unsigned int global_internal = (int)config["trigger_acquisition"]["internal"];
  unsigned int global_external = (int)config["trigger_acquisition"]["external"];
  unsigned int global_software = (int)config["trigger_acquisition"]["software"];
  
  DEBUG("LVDS     : "<<global_lvds<<" [hardcoded for FASER]"); 
  DEBUG("Internal : "<<global_internal);
  DEBUG("External : "<<global_external);
  DEBUG("Software : "<<global_software);
  

  // Global Trigger Mask
  // this is the generation of a global OR that will 
  // be used to generate an acquisition of a single event
  unsigned int trig_enable_mask;
  ReadSlaveReg(m_crate, m_base_address + VX1730_TRIG_SRCE_EN_MASK, trig_enable_mask, true );
  DEBUG("Initial TrigEnableMask : "<< ConvertIntToWord(trig_enable_mask));

  // enable each group separately
  for(int iGrp=0; iGrp<config["trigger_internal"].size(); iGrp++){
    int group  = (int)config["trigger_internal"].at(iGrp)["group"];
    int enable = (int)config["trigger_internal"].at(iGrp)["enable"];
    
    DEBUG("Trigger group : "<<group<<"  - Enable bit : "<<enable);
    
    if(global_internal==0){
      WARNING("Internal acquisition is globally disabled - setting to FALSE for group "<<group);
    }
    
    SetWordBit(trig_enable_mask, group, enable);
  }

  // change the global features  
  SetWordBit(trig_enable_mask, 29, 0); // hardcoded since we don't use it in FASER
  SetWordBit(trig_enable_mask, 30, global_external);
  SetWordBit(trig_enable_mask, 31, global_software);
  DEBUG("Final TrigEnableMask : "<<std::hex<<trig_enable_mask);

  WriteSlaveReg(m_crate, m_base_address + VX1730_TRIG_SRCE_EN_MASK, trig_enable_mask, true );
  ReadSlaveReg(m_crate, m_base_address + VX1730_TRIG_SRCE_EN_MASK, trig_enable_mask, true );
}

/*!
 * \brief Configure trigger groups
 *
 * The 16 channels of the digitizer are divided into 8 pairs of channels, each one of which is responsible for generating a trigger signal. 
 * If configured, these signals can generate an acquisition trigger or be sent to the LVDS bits for hardware transmission of trigger
 * data as in FASER.  This method configures, for all channels and groups :
 *   - [1] individual channel thresholds
 *   - [2] logical combination of both channels in the group
 *   - [3] output width of trigger signal sent out of that channel group
 *
 * \param[in] [config] [The json configuration object that contains the desired settings for data taking.]
 * \param[in] [debug]  [A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.
 * \callgraph
 * \callergraph
 * \showrefby
 */
void vx1730::ConfigGroupTriggerSettings( json config, bool debug){
  INFO("Configuring trigger group settings (thresholds, output pulse width)");
  
  /////////////////////////////////////////////////////////////
  // trigger polarity - asset trigger on rising or falling breach of threshold
  /////////////////////////////////////////////////////////////
  DEBUG("Trigger polarity for all channels");
  
  unsigned int channel_config;
  ReadSlaveReg(m_crate, m_base_address + VX1730_CHANNEL_CONFIG, channel_config, true );

  DEBUG("Initial ChannelConfig : "<< ConvertIntToWord(channel_config));

  DEBUG("Setting trigger polarity to default OVER");
  DEBUG("You will be notified if it is set to under");
  SetWordBit(channel_config, 6, 0);
  if(config["trigger_internal_polarity"]=="under"){
    DEBUG("Setting trigger polarity to UNDER");
    SetWordBit(channel_config, 6, 1);
  }

  DEBUG("Final ChannelConfig : "<< ConvertIntToWord(channel_config));
  
  unsigned int channel_config_check;

  WriteSlaveReg(m_crate, m_base_address + VX1730_CHANNEL_CONFIG, channel_config, true );
  ReadSlaveReg(m_crate, m_base_address+VX1730_CHANNEL_CONFIG, channel_config_check, debug );
  
  
  /////////////////////////////////////////////////////////////
  // trigger group settings
  //   [1] individual channel thresholds
  //   [2] logical combination of both channels in the group
  //   [3] output width of trigger signal sent out of that channel group
  /////////////////////////////////////////////////////////////
  DEBUG("Trigger Thresholds - once for each channel");
  
  // to check which trigger thresholds have been set
  bool trig_set[8]={false,false,false,false,false,false,false,false};
  
  for(int iGrp=0; iGrp<config["trigger_internal"].size(); iGrp++){

    int group = config["trigger_internal"].at(iGrp)["group"];
    DEBUG("Group : "<<iGrp<<"  "<<group);
    
    // verify that you set the thresholds on all channels but not double
    if(trig_set[group]==false){
      trig_set[group]=true;
    }
    else{
      WARNING("You are trying to reset the trigger threshold for group : "<<group);
    }
      
    // set the two channels in this trigger group
    int channel = 0;
    unsigned int threshold = 0;
    
    INFO("Setting Thresholds");

    // first channel in group
    channel   = (int)config["trigger_internal"].at(iGrp)["group"]*2;
    threshold = (unsigned int)config["trigger_internal"].at(iGrp)["threshold_ch0"];
    SetTriggerChannelThreshold(channel, threshold);
  
    // second channel in group
    channel   = (int)config["trigger_internal"].at(iGrp)["group"]*2 + 1;
    threshold = (unsigned int)config["trigger_internal"].at(iGrp)["threshold_ch1"];
    SetTriggerChannelThreshold(channel, threshold);
    
    INFO("Setting Logic");

    // logic about how to combine the information from the two channels in a group
    DEBUG("Setting group pair trigger logic combination");
    std::string trig_logic = std::string(config["trigger_internal"].at(iGrp)["logic"]);
    DEBUG("Trig group logic setting : "<<group<<"  "<<trig_logic);
    
    unsigned int trig_chan_logic;
    ReadSlaveReg(m_crate, m_base_address + VX1730_CHANNEL_TRIG_LOGIC + (0x0100)*group, trig_chan_logic, true);
    DEBUG("Trigger group logic Init : "<<ConvertIntToWord(trig_chan_logic));
    
    if( trig_logic.compare("AND")==0 ){
      SetWordBit(trig_chan_logic,0,0);
      SetWordBit(trig_chan_logic,1,0);
    }
    else if( trig_logic.compare("OnlyA")==0 ){
      SetWordBit(trig_chan_logic,0,1);
      SetWordBit(trig_chan_logic,1,0);
    }
    else if( trig_logic.compare("OnlyB")==0 ){
      SetWordBit(trig_chan_logic,0,0);
      SetWordBit(trig_chan_logic,1,1);
    }
    else if( trig_logic.compare("OR")==0 ){
      SetWordBit(trig_chan_logic,0,1);
      SetWordBit(trig_chan_logic,1,1);
    } 
    else{
      THROW(DigitizerHardwareException, "This is not a proper trigger logic configuration");
    }
    
    // always set bit[2]=0 so that the output trigger is of configurable width as programmed next
    SetWordBit(trig_chan_logic,2,0); // configurable width
        
    DEBUG("Trigger group logic Final : "<<ConvertIntToWord(trig_chan_logic));
    
    // note that the actual registers are in sets of two and so the iterator needs to be multiplied by 2
    WriteSlaveReg(m_crate, m_base_address + VX1730_CHANNEL_TRIG_LOGIC + (0x0100)*(2*group), trig_chan_logic, true);
    ReadSlaveReg(m_crate, m_base_address + VX1730_CHANNEL_TRIG_LOGIC + (0x0100)*(2*group), trig_chan_logic, true);


    // configured width of the trigger window output pulse length
    // need to set the width to be the same in both channels of the group
    DEBUG("Setting group trigger pulse output width");
    unsigned int trig_output_width = (int)config["trigger_internal"].at(iGrp)["output_width"];
    DEBUG("Output width : "<<trig_output_width);
    
    SetTriggerGroupOutputWidth((group*2)  , trig_output_width);
    SetTriggerGroupOutputWidth((group*2)+1, trig_output_width);
  }
}

/*!
 * \brief Configure trigger coincidence
 *
 * For standalone usage, a coincidence among trigger groups can be configured if the digitizer has been configured to record data in self-triggering acquisition mode.
 *
 * \param[in] [config] [The json configuration object that contains the desired settings for data taking.]
 * \param[in] [debug]  [A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.
 * \note This is not necessary for the application to FASER but stays here for standalone usage 
 * \callgraph
 * \callergraph
 * \showrefby
 */
void vx1730::ConfigTriggerCoincidence( json config, bool debug){
  INFO("Configuring trigger coincidence settings");

  auto cfg_coincidence = config["trigger_coincidence"]["coincidence"];
  if (cfg_coincidence!="" && cfg_coincidence!=nullptr){
    ERROR("Missing config[\"trigger_coincidence\"][\"coincidence\"]");
    THROW(DigitizerHardwareException, "Missing configuration for setting coincidence");
  }

  cfg_coincidence = config["trigger_coincidence"]["nchan"];
  if (cfg_coincidence!="" && cfg_coincidence!=nullptr){
    ERROR("Missing config[\"trigger_coincidence\"][\"nchan\"]");
    THROW(DigitizerHardwareException, "Missing configuration for setting coincidence");
  }

  cfg_coincidence = config["trigger_coincidence"]["timewindow"];
  if (cfg_coincidence!="" && cfg_coincidence!=nullptr){
    ERROR("Missing config[\"trigger_coincidence\"][\"timewindow\"]");
    THROW(DigitizerHardwareException, "Missing configuration for setting coincidence");
  }

  unsigned int trig_enable_mask;
  ReadSlaveReg(m_crate, m_base_address + VX1730_TRIG_SRCE_EN_MASK, trig_enable_mask, true );
  DEBUG("Initial TrigEnableMask : "<< ConvertIntToWord(trig_enable_mask));

  // if you want to set a multichannel coincidence
  if( (bool)config["trigger_coincidence"]["coincidence"] ){
    
    // how many channel-pairs enter the coincidence from among the enabled channels (set above)
    // the number in these three bits is one less than the number of channels in the coincidence
    if( (int)config["trigger_coincidence"]["nchan"]<0 || (int)config["trigger_coincidence"]["nchan"]>8 ){
      THROW(DigitizerHardwareException, "You can't have that number of coincidences : "+to_string((int)config["trigger_coincidence"]["nchan"]));
    }
    else{
    
      DEBUG("trig_enable_mask1[bit] : "<<ConvertIntToWord(trig_enable_mask));
      
      // set the necessary bits to 0
      SetWordBit(trig_enable_mask, 26, 0);
      SetWordBit(trig_enable_mask, 25, 0);
      SetWordBit(trig_enable_mask, 24, 0);

      // make the mask for the OR
      unsigned int nchan = ( ((int)config["trigger_coincidence"]["nchan"]-1) << 24);
      
      DEBUG("NChan[bit] : "<<ConvertIntToWord(nchan));
      
      // take the OR
      trig_enable_mask |= nchan;
    
      DEBUG("trig_enable_mask2[bit] : "<<ConvertIntToWord(trig_enable_mask));
    
    }
    
    // what is the duration of the coincidence window
    // in units of [nanoseconds] - rounding down to the nearest 8ns block
    // bits[23:20] = 8+4+2+1 = 15 is the max number of clock cycles
    // 15*8ns = 120ns
    if( (int)config["trigger_coincidence"]["timewindow"]>120 ){
      THROW(DigitizerHardwareException, "This coincidence time window is too long (max = 120ns) : "+to_string((int)config["trigger_coincidence"]["timewindow"]));
    }
    else{
      int nclock = floor( (int)config["trigger_coincidence"]["timewindow"]/8.0 );
      DEBUG("NClock for Coincidence : "<<nclock);
      DEBUG("NClock[hex] : "<<std::hex<<nclock);
      
      DEBUG("trig_enable_mask1[bit] : "<<ConvertIntToWord(trig_enable_mask));
      
      // set the necessary bits to 0
      SetWordBit(trig_enable_mask, 23, 0);
      SetWordBit(trig_enable_mask, 22, 0);
      SetWordBit(trig_enable_mask, 21, 0);
      SetWordBit(trig_enable_mask, 20, 0);
      
      // make the mask for the OR
      unsigned int twin = (nclock<<20);
      
      DEBUG("TWin[bit] : "<<ConvertIntToWord(twin));
      
      // take the OR
      trig_enable_mask |= twin;
    
      DEBUG("trig_enable_mask2[bit] : "<<ConvertIntToWord(trig_enable_mask));
    }
  }
  else{
    DEBUG("You are not doing multi-channel coincidence in the global readout trigger");
  }

  DEBUG("Final TrigEnableMask : "<<std::hex<<trig_enable_mask);

  WriteSlaveReg(m_crate, m_base_address + VX1730_TRIG_SRCE_EN_MASK, trig_enable_mask, true );
  ReadSlaveReg(m_crate, m_base_address + VX1730_TRIG_SRCE_EN_MASK, trig_enable_mask, true );
}

/*!
 * \brief Configure TRG-OUT front panel
 *
 * Configures the behavior of the front panel TRG-OUT signal
 *
 * \param[in] [config] [The json configuration object that contains the desired settings for data taking.]
 * \param[in] [debug]  [A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.
 * \note This is not used by FASER
 * \callgraph
 * \callergraph
 * \showrefby
 */
void vx1730::ConfigFrontPanelTrigger( json config, bool debug){
  INFO("Configuring front panel trigger");

  DEBUG("TrigOut Enable Mask");
  
  unsigned int trig_out_enable_mask = 0;
  ReadSlaveReg(m_crate, m_base_address+VX1730_FP_TRIGGER_OUT_EN_MASK, trig_out_enable_mask, true );
  
  // currently hardcoded but this is not used in FASER
  SetWordBit(trig_out_enable_mask,0,1);
  SetWordBit(trig_out_enable_mask,29,0);
  SetWordBit(trig_out_enable_mask,30,0);
  SetWordBit(trig_out_enable_mask,31,0);
  
  WriteSlaveReg(m_crate, m_base_address+VX1730_FP_TRIGGER_OUT_EN_MASK, trig_out_enable_mask, true );
  ReadSlaveReg(m_crate, m_base_address+VX1730_FP_TRIGGER_OUT_EN_MASK, trig_out_enable_mask, true );
  DEBUG("TrigOutEnable : "<< ConvertIntToWord(trig_out_enable_mask));
}

/*!
 * \brief Configure LVDS bits
 *
 * This configures the behavior of the LVDS hardware signals that are used in FASER to 
 * transmit data from the digitizer to the Trigger Logic Board.  Currently, everything here is
 * hardcoded due to the specifications of FASER and send the following output signals
 *   - bits[3,0] : Trigger signal from trigger groups [3,0] (i.e. channels {7,0}) - active HIGH
 *   - bits[7,4] : Trigger signal from trigger groups [7,4] (i.e. channels {15,8}) - active HIGH
 *   - bit[8] : The busy signal, which is asserted as active LOW once the number of events in the hardware buffer breaches a configurable threshold.  The maximum number of events that can be stored on digitizer is 1024, independent of the acquisition window size.
 *
 * \param[in] [config] [The json configuration object that contains the desired settings for data taking.]
 * \param[in] [debug]  [A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.
 * \callgraph
 * \callergraph
 * \showrefby
 */
void vx1730::ConfigLVDS( json config, bool debug){
  INFO("Configuring the LVDS output pins");

  // 0x811C - Front Panel I/O Control
  unsigned int frontpanel_control;
  ReadSlaveReg(m_crate, m_base_address+VX1730_FP_IO_CONTROL, frontpanel_control);
  DEBUG("LVDS I/O Control Init  : "<< ConvertIntToWord(frontpanel_control));
  
  // LEMO I/O signal - needed for S-IN signal type
  SetWordBit(frontpanel_control, 0, 1); 

  // FASER specific in that we are settings pins [0,7] to sent output
  SetWordBit(frontpanel_control, 2, 1); // LVDS[0,3]   set to output - bits sent to FASER/TLB for trigger decision
  SetWordBit(frontpanel_control, 3, 1); // LVDS[4,7]   set to output - bits sent to FASER/TLB for trigger decision 
  SetWordBit(frontpanel_control, 4, 1); // LVDS[8,11]  set to output - bits sent to FASER/TLB for hardware busy signal
  SetWordBit(frontpanel_control, 5, 1); // LVDS[12,15] set to output - NOT USED (see below)
  
  // Enable new features (recommended by CAEN documentation) 
  // Allows for toggling of *what* information is being sent on the LVDS pins 
  // requires subsequent configuration of 0x81A0 which is performed below on VX1730_FP_LVDS_IO_CTRL
  SetWordBit(frontpanel_control, 8, 1); 
  
  // TRIG-IN configuration
  SetWordBit(frontpanel_control, 10, 0); // trigger occurs on rising edge of input
  SetWordBit(frontpanel_control, 11, 0); // TRG-IN is first processed by motherboard --> this may induce more delay
  
// The TRG-OUT is not used for anything so it is not configured beyond its default
//  // TRG-OUT configuration
//  // level of test bit --> only matters is bit [15] is set to (1)
//  SetWordBit(frontpanel_control, 14, 0); 
//  // is TRG-OUT a (1) logic level or (0) an internal generation
//  SetWordBit(frontpanel_control, 15, 0); 
//  // TRG-OUT mode selection
//  //  00 = Trigger: TRG‐OUT/GPO propagates the internal trigger sources according to register 0x8110;
//  //  01 = Motherboard Probes: TRG‐OUT/GPO is used to propagate signals of the motherboards according to bits[19:18];
//  //  10 = Channel Probes: TRG‐OUT/GPO is used to propagate signals of the mezzanines (Channel Signal Virtual Probe);
//  //  11 = S‐IN (GPI) propagation.
//  SetWordBit(frontpanel_control, 16, 0); 
//  SetWordBit(frontpanel_control, 17, 0); 
//  // Motherboard Virtual Probe Selection
//  SetWordBit(frontpanel_control, 18, 0); 
//  SetWordBit(frontpanel_control, 19, 0); 
   
  WriteSlaveReg(m_crate, m_base_address+VX1730_FP_IO_CONTROL, frontpanel_control);
  ReadSlaveReg(m_crate, m_base_address+VX1730_FP_IO_CONTROL, frontpanel_control);
  DEBUG("LVDS I/O Control Final : "<< ConvertIntToWord(frontpanel_control));
  
  // 0x81A0 - Front Panel LVDS I/O New Features
  unsigned int lvds_output_config;
  ReadSlaveReg(m_crate, m_base_address+VX1730_FP_LVDS_IO_CTRL, lvds_output_config);
  DEBUG("LVDS IO Config Init  : "<< ConvertIntToWord(lvds_output_config));
  
  // Options are the following for grouped bits [N+3,N] for group N :
  // 1) 0000 = REGISTER, where the four LVDS I/O pins act as register (read/write 
  //           according to the configured input/output option);
  // 2) 0001 = TRIGGER, where each group of four LVDS I/O pins can be configured to 
  //           receive an input trigger for each channel (DPP Firmware only), or to propagate 
  //           out the trigger request;
  // 3) 0010 = nBUSY/nVETO, where each group of four LVDS I/O pins can be configured 
  //           as inputs (0 = nBusyIn, 1 = nVetoIn, 2 = nTrigger In, 3 = nRun In) or 
  //           as outputs (0 = nBusy, 1 = nVeto, 2 = nTrigger Out, 3 = nRun );
  // 4) 0011 = LEGACY, that is to say according to the old LVDS I/O configuration 
  //           (i.e. ROC FPGA firmware revisions lower than 3.8), where the LVDS can 
  //           be configured as 0 = nclear TTT, and 1 = 2 = 3 = reserved in case of 
  //           input LVDS setting, while they can be configured as 0 = Busy, 1 = Data 
  //           ready, 2 = Trigger, 3 = Run in case of output LVDS setting. Please refer 
  //           to the Front Panel LVDS I/Os section of the digitizer User Manual for 
  //           detailed description. NOTE: LVDS I/O new features are supported from ROC 
  //           FPGA firmware revision 3.8 on. NOTE: this register is supported by VME boards only.
  
  // output of pins LVDS pins [3,0] which correspond to the associated trigger groups
  SetWordBit(lvds_output_config, 0, 1);
  SetWordBit(lvds_output_config, 1, 0);
  SetWordBit(lvds_output_config, 2, 0);
  SetWordBit(lvds_output_config, 3, 0);
  
  // output of pins LVDS pins [7,4] which correspond to the associated trigger groups
  SetWordBit(lvds_output_config, 4, 1);
  SetWordBit(lvds_output_config, 5, 0);
  SetWordBit(lvds_output_config, 6, 0);
  SetWordBit(lvds_output_config, 7, 0);  
  
  // output of pins LVDS pins [11,8] are used to send the nBUSY/nVETO for almost full level 
  SetWordBit(lvds_output_config, 8,  0);
  SetWordBit(lvds_output_config, 9,  1);
  SetWordBit(lvds_output_config, 10, 0);
  SetWordBit(lvds_output_config, 11, 0);  
  
  // output of pins LVDS pins [15,12] are NOT used
  SetWordBit(lvds_output_config, 12, 0);
  SetWordBit(lvds_output_config, 13, 0);
  SetWordBit(lvds_output_config, 14, 0);
  SetWordBit(lvds_output_config, 15, 0);  
  
  // write the config to the proper register
  DEBUG("About to write : "<< ConvertIntToWord(lvds_output_config));
  WriteSlaveReg(m_crate, m_base_address+VX1730_FP_LVDS_IO_CTRL, lvds_output_config);
  ReadSlaveReg(m_crate, m_base_address+VX1730_FP_LVDS_IO_CTRL, lvds_output_config);
  DEBUG("LVDS IO Config Final : "<< ConvertIntToWord(lvds_output_config));
  
  
  
  // set value for almost full level
  unsigned int almost_full_level;
  ReadSlaveReg(m_crate, m_base_address+VX1730_ALMOST_FULL_LEVEL, almost_full_level);
  DEBUG("Buffer Busy Almost Full Level[initial] : "<< ConvertIntToWord(almost_full_level));
  
  almost_full_level = (unsigned int)config["readout"]["n_busy_level"];
  
  if(almost_full_level<=0){
    WARNING("Must set full level to a positive value between [0,1024] - setting to max 1024");
    almost_full_level=1024;
  }
  else if(almost_full_level>=1024){
    WARNING("Must set full level to a positive value between [0,1024] - setting to max 1024");
    almost_full_level=1024;
  }
  else{
    INFO("Setting fill level to : "<<almost_full_level);
  }
  WriteSlaveReg(m_crate, m_base_address+VX1730_ALMOST_FULL_LEVEL, almost_full_level);
  ReadSlaveReg(m_crate, m_base_address+VX1730_ALMOST_FULL_LEVEL, almost_full_level);
  DEBUG("Buffer Busy Almost Full Level[final]  : "<< ConvertIntToWord(almost_full_level));
  
}

/*!
 * \brief Enable internal sawtooth
 *
 * Configuration to enable an internally generated sawtooth pattern for exercising readout capabilities.  
 *
 * \param[in] [config] [The json configuration object that contains the desired settings for data taking.]
 * \param[in] [debug]  [A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.
 * \note This is not used in FASER but stays here for standalone usage
 * \callgraph
 * \callergraph
 * \showrefby
 */
void vx1730::ConfigInternalSawtooth( json config, bool debug){
  INFO("Configuring internal sawtooth signal");
  
  unsigned int channel_config;
  ReadSlaveReg(m_crate, m_base_address + VX1730_CHANNEL_CONFIG, channel_config, true );

  DEBUG("Initial ChannelConfig : "<< ConvertIntToWord(channel_config));

  DEBUG("SawTooth Enable : "<<config["internal_sawtooth"]);
  unsigned int sawtooth_enable;
  if(config["internal_sawtooth"]==true){
    DEBUG("Turning ON internal sawtooth waveform");
    SetWordBit(channel_config, 3, 1);
  }
  else if(config["internal_sawtooth"]==false){
    DEBUG("Turning OFF internal sawtooth waveform");
    SetWordBit(channel_config, 3, 0);
  }  

  DEBUG("Final ChannelConfig : "<< ConvertIntToWord(channel_config));
  
  unsigned int channel_config_check;

  WriteSlaveReg(m_crate, m_base_address + VX1730_CHANNEL_CONFIG, channel_config, true );
  ReadSlaveReg(m_crate, m_base_address+VX1730_CHANNEL_CONFIG, channel_config_check, debug );
}

/*!
 * \brief Board reset
 *
 * Performs a reset of the board, but not of each of the ADCs.
 *
 * \param[in] [debug]  [A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.
 * \note This is not used by FASER
 * \callgraph
 * \callergraph
 * \showrefby
 * \todo Is this redundant with the ConfigReset() method and can be removed?
 */
void vx1730::Reset( bool debug){
  INFO("Performing full board reset");

  // perform system reset
  DEBUG("Reset : System configurations");
  WriteSlaveReg(m_crate, m_base_address+VX1730_SW_RESET, 0x1111, debug );

  DEBUG("Reset : Clear memory buffers");
  WriteSlaveReg(m_crate, m_base_address+VX1730_SW_CLEAR, 0x1111, debug );
}

/*!
 * \brief Begins data acquisition
 *
 * Begins the acquisition of data by turning on the functioning of the ADCs and hardware buffers.  Only after executing this method will the digitizer store any data upon receiving an acquisition trigger signal of any type, internally or externally generated.
 *
 * \param[in] [debug]  [A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.
 * \note This should be done only after performing the board configuration using your input configuration file to ensure you know what state the board is in.
 * \callgraph
 * \callergraph
 * \showrefby
 */
void vx1730::StartAcquisition( bool debug ){
  INFO("Starting data acquisition");
  
  unsigned int data;

  ReadSlaveReg(m_crate, m_base_address + VX1730_ACQUISITION_CONTROL, data, debug );
  DEBUG("StartAcquireState[1]: "<<ConvertIntToWord(data));

  WriteSlaveReg(m_crate, m_base_address + VX1730_ACQUISITION_CONTROL, 0x04, debug );

  ReadSlaveReg(m_crate, m_base_address + VX1730_ACQUISITION_CONTROL, data, debug );
  DEBUG("StartAcquireState[2]: "<<ConvertIntToWord(data));

  // sleep for a short while before data taking starts
  usleep(10000);
}

/*!
 * \brief Stops data acquisition
 *
 * Ceases the ADC and hardware buffer and therefore the storage of new events upon reception of an acquisition trigger.  However, the hardware buffer persists after executing this method and so there may still be events present which can (and should) be read out afterwards.
 *
 * \param[in] [debug]  [A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.
 * \callgraph
 * \callergraph
 * \showrefby
 */
void vx1730::StopAcquisition( bool debug ){
  INFO("Stopping data acquisition");
  
  unsigned int data;

  ReadSlaveReg(m_crate, m_base_address + VX1730_ACQUISITION_CONTROL, data, debug );
  WriteSlaveReg(m_crate, m_base_address + VX1730_ACQUISITION_CONTROL, 0x00, debug );
  ReadSlaveReg(m_crate, m_base_address + VX1730_ACQUISITION_CONTROL, data, debug );
}

/*!
 * \brief Calibrate the ADC
 *
 * Starts the calibration procedure for the ADCs on board.
 *
 * \param[in] [debug]  [A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.
 * \note This is only meaningful/necessary for older versions of the board.  This is relevant in the case of FASER in which the digitizer in the scilab is a vx1725 board and therefore *needs* to be calibrated each time it is powered on or in the case of temperature changes.  The digitizer board in EHN1, and the experimental cavern, is of type vx1730 and performs a self-calibration procedure upon startup.
 * \warning The actual calibration procedure requires that one query each ADC until it is finished.  As such, this method should only be used as implemented in the CalibrateADC.cpp utility.
 * \callgraph
 * \callergraph
 * \showrefby
 */
void vx1730::ADCCalibration( bool debug ){
  INFO("Performing calibration of ADCs on board");
  WriteSlaveReg(m_crate, m_base_address+VX1730_ADC_CALIBRATION, 0x1 );
}

/*!
 * \brief Get ADC temperatures
 *
 * This can be used to monitor the temperature of the ADCs, recorded and returned in degrees celsius.
 *
 * \param[in] [debug]  [A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.]
 * \param[out] [all_temperatures]  [A vector of the temperature of the 16 ADCs on the board.
 * \callgraph
 * \callergraph
 * \showrefby
 */
std::vector<int> vx1730::GetADCTemperature(bool debug){
  DEBUG("Getting temperature of all ADCs in board");
  
  unsigned int data;
  int temperature;
  std::vector<int> all_temperature;

  for(int iChan=0; iChan<NCHANNELS; iChan++){
    ReadSlaveReg(m_crate, m_base_address + VX1730_CHANNEL_TEMPERATURE + 0x0100*iChan, data, debug );
    temperature = data & 0x000000FF;
    all_temperature.push_back(temperature);
  }
  
  if(all_temperature.size()!=NCHANNELS){
    ERROR("Your temperature monitoring shows an incorrect number of channels : "<<NCHANNELS);
  }
  
  return all_temperature;
}

/*!
 * \brief Monitor the ADC temperatures
 *
 * A wrapper of the temperature reading that can be used to dump the temperatures to a text file.
 *
 * \param[in] [outputfile] [The file in which to store the recorded ADC temperatures]
 * \param[in] [debug] [A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.]
 * \note This is not used by FASER
 * \callgraph
 * \callergraph
 * \showrefby
 */
void vx1730::MonitorTemperature( std::string outputfile, bool debug){
  INFO("Dumping temperature information to text file");

  ofstream fout;
  fout.open(outputfile, ios::out);

  // write data for each channel that is active and -1 if its not active
  fout<<setw(10)<<"reading";
  for(int iChan=0; iChan<NCHANNELS; iChan++){
    std::string line = "chan-"+to_string(iChan);
    fout<<setw(10)<<line;
  }
  fout<<"\n";

  unsigned int data;
  int temperature;

  for(int iRead=0; iRead<30; iRead++){
    fout<<setw(10)<<std::dec<<iRead;
    for(int iChan=0; iChan<NCHANNELS; iChan++){
      ReadSlaveReg(m_crate, m_base_address + VX1730_CHANNEL_TEMPERATURE + 0x0100*iChan, data, debug );
      temperature = data & 0x000000FF;
      fout<<setw(10)<<std::dec<<temperature;
    }

    usleep(10000);
  }

  fout.close();
}

/*!
 * \brief Read and show the vx1730 register settings
 *
 * Show the relevant bits of the configuration that is currently set on the vx1730.  This does not show the entire configuration, but only those that have been changed for use in FASER.
 *
 * \callgraph
 * \callergraph
 * \showrefby
 */
void vx1730::DumpConfig(){
  INFO("========================================");
  INFO("Dumping board configuration : ");

  // reuse this same value for reading
  unsigned int data;
  unsigned int rev_minor;
  unsigned int rev_major;
  unsigned int rev_date;

  // board info - e.g. firmware versions
  INFO("\n\n-- Board Info :");

  ReadSlaveReg(m_crate, m_base_address+VX1730_ROC_FPGA_FW_REV, data);
  INFO("Firmware for ROC FPGA Motherboard  ("<<std::hex<<VX1730_ROC_FPGA_FW_REV<<") [hex]: 0x"<<std::hex<<data);
  rev_minor = (data & 0x000000FF);
  rev_major = (data & 0x0000FF00) >> 8;
  rev_date  = (data & 0xFFFF0000) >> 16;
  INFO("Revision Minor  : "<<std::dec<<rev_minor);
  INFO("Revision Major  : "<<std::dec<<rev_major);
  INFO("Revision Date   : "<<std::dec<<rev_date);
 
  // VX1730_CHANNEL_AMC_FIRMWARE_VERSION+ichan*(0x0100)
  INFO("Firmware per ADC Channel :");
  for(int iChan=0; iChan<NCHANNELS; iChan++){
    ReadSlaveReg(m_crate, m_base_address+VX1730_CHANNEL_AMC_FIRMWARE_VERSION+(0x0100*iChan), data);

    rev_minor = (data & 0x000000FF);
    rev_major = (data & 0x0000FF00) >> 8;
    rev_date  = (data & 0xFFFF0000) >> 16;

    INFO("CHANNEL_FW "<<iChan<<"      ("<<std::hex<<VX1730_CHANNEL_AMC_FIRMWARE_VERSION+(0x0100*iChan)<<") [hex]: "<<std::hex<<data<<" (min,maj,date) :"<<std::dec<<rev_minor<<","<<rev_major<<","<<rev_date);   
  }

  // global information
  INFO("\n\n-- Global Info :");
  ReadSlaveReg(m_crate, m_base_address+VX1730_CUSTOM_SIZE, data);
  if(data==0x0){
    ReadSlaveReg(m_crate, m_base_address+VX1730_BUFFER_ORGANIZATION, data);
    INFO("BUFFER_ORGANIZATION : Fixed Length");
    INFO("BUFFER_ORGANIZATION  ("<<std::hex<<VX1730_BUFFER_ORGANIZATION<<") [hex]: 0x"<<std::hex<<data<<"  -  nsamples="<<std::dec<<GetBufferLength(data));
  }
  else{
    ReadSlaveReg(m_crate, m_base_address+VX1730_CUSTOM_SIZE, data);
    INFO("BUFFER_ORGANIZATION : Custom Length");
    INFO("BUFFER_ORGANIZATION  ("<<std::hex<<VX1730_BUFFER_ORGANIZATION<<") [hex]: 0x"<<std::hex<<data<<"  -  nsamples="<<std::dec<<data*10);  
  }
  
  ReadSlaveReg(m_crate, m_base_address+VX1730_POST_TRIGGER_SETTING, data);
  INFO("POST_TRIGGER_SETTING ("<<std::hex<<VX1730_POST_TRIGGER_SETTING<<") [dec]: "<<std::hex<<data);

  ReadSlaveReg(m_crate, m_base_address+VX1730_CHANNEL_CONFIG, data);
  INFO("CHANNEL_CONFIG       ("<<std::hex<<VX1730_CHANNEL_CONFIG<<") [bit]: "<<std::hex<<ConvertIntToWord(data));

  ReadSlaveReg(m_crate, m_base_address+VX1730_ACQUISITION_CONTROL, data); 
  INFO("ACQUISITION_CONTROL  ("<<std::hex<<VX1730_ACQUISITION_CONTROL<<") [bit]: "<<std::hex<<ConvertIntToWord(data));

  // readout info
  INFO("\n\n-- Readout Enable :");
  ReadSlaveReg(m_crate, m_base_address+VX1730_CHANNEL_EN_MASK, data);
  INFO("CHANNEL_EN_MASK      ("<<std::hex<<VX1730_CHANNEL_EN_MASK<<") [bit]: "<<std::hex<<ConvertIntToWord(data));

  INFO("\n\n-- Channel DAC :");
  for(int iChan=0; iChan<NCHANNELS; iChan++){
    ReadSlaveReg(m_crate, m_base_address+VX1730_CHANNEL_DAC+(0x0100*iChan), data);
    INFO("CHANNEL_DAC "<<iChan<<"      ("<<std::hex<<VX1730_CHANNEL_DAC+(0x0100*iChan)<<") [hex]: "<<std::hex<<data);
  }

  // trigger info
  INFO("\n\n-- Triggering :");
  ReadSlaveReg(m_crate, m_base_address+VX1730_TRIG_SRCE_EN_MASK, data);
  INFO("TRIG_SRCE_EN_MASK       ("<<std::hex<<VX1730_TRIG_SRCE_EN_MASK<<") [hex]: "<<std::hex<<data);
  INFO("TRIG_SRCE_EN_MASK       ("<<std::hex<<VX1730_TRIG_SRCE_EN_MASK<<") [bit]: "<<std::hex<<ConvertIntToWord(data));

  // 0x811C
  unsigned int frontpanel_control;
  ReadSlaveReg(m_crate, m_base_address+VX1730_FP_IO_CONTROL, frontpanel_control);
  DEBUG("FrontPanel I/O Control : "<<ConvertIntToWord(frontpanel_control));
  DEBUG("Lemo IO Levels         : "<<GetWordBit(frontpanel_control,0));

  unsigned int lvds_output_config;  
  ReadSlaveReg(m_crate, m_base_address+VX1730_FP_LVDS_IO_CTRL, lvds_output_config);
  DEBUG("LVDS IO Config         : "<< ConvertIntToWord(lvds_output_config));

  for(int iChan=0; iChan<NCHANNELS; iChan++){
    ReadSlaveReg(m_crate, m_base_address+VX1730_CHANNEL_TRIG_THRESH+(0x0100*iChan), data);
    INFO("TRIGGER_THRESH "<<iChan<<"      ("<<std::hex<<VX1730_CHANNEL_TRIG_THRESH+(0x0100*iChan)<<") [hex]: "<<std::hex<<data);
  }

  for(int iChan=0; iChan<(int)(NCHANNELS/2.0); iChan++){
    ReadSlaveReg(m_crate, m_base_address+VX1730_CHANNEL_TRIG_PULSE_WIDTH+(0x0100*iChan), data);
    INFO("TRIGGER_PULSE_WIDTH(group) "<<iChan<<"      ("<<std::hex<<VX1730_CHANNEL_TRIG_PULSE_WIDTH+(0x0100*iChan)<<") [hex]: "<<std::hex<<data);
  }
  
  INFO("Finished dumping board configuration : ");
  INFO("========================================");
  
}

/*!
 * \brief Read single event to text file
 *
 * Reads out a single event and stores it in a text file.  This can be done in a completely new text file, or by adding this event to an existing text file, depending on the mode of reading specified.
 *
 * \param[in] [outputfile] [The path to the text file where you wish to store the event.  The path to the text file must exist.]
 * \param[in] [mode] [One of The enum DumpMode which can take values {New, Append} and refers to whether the file will be freshly created, or each subsequent event will be appended onto the same text file.]
 * \param[in] [debug] [A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.]

 * \note This is not used by FASER and is preserved for standalone running
 * \callgraph
 * \callergraph
 * \showrefby
 */
void vx1730::DumpFrontEvent( std::string outputfile, DumpMode mode, bool debug ){
  DEBUG("vx1730::DumpFrontEvent");

  // FIXME : these are variables that should be controlled by DAQ
  uint32_t source_id = 0;
  uint64_t event_id  = 0;

  /////////////////////////////////
  // get the event
  /////////////////////////////////
  DEBUG("Get the Event");
  uint32_t raw_payload[MAXFRAGSIZE];
  this->ReadEventSingle( raw_payload, debug );

  if(debug){
    DEBUG("Print the header manually for debugging");
    for(int iWord=0; iWord<4; iWord++){
      DEBUG("Word : "<<iWord<<"  "<<raw_payload[iWord]);
    }
  }
  
  if(debug){
    DEBUG("Dumping payload to check");
    Payload_Dump( raw_payload );
  }

  DumpEventCount( debug );

  /////////////////////////////////
  // format the event by putting the FASER header on it
  /////////////////////////////////
  DEBUG("Format the event by putting the FASER header on it");
  int payload_size = Payload_GetEventSize( raw_payload );

  if(debug){
    DEBUG("Raw Payload size           : "<<std::dec<<payload_size);
  }

  // event fragment creation
  std::unique_ptr<EventFragment> data(new EventFragment(0, source_id, event_id, 0xFFFF, raw_payload,payload_size * 4));

  /////////////////////////////////
  // check that it copied by dumping again
  /////////////////////////////////
  DEBUG("Check that it copied by dumping again");
  if(debug)
    Data_Dump( data, 5, debug );

  Data_Write( data, outputfile, mode);

}

/*!
 * \brief Mimic of FASER module
 *
 * Intended as a mimic of what is done in the FASER module for handling a single event
 *
 * \param[in] [raw_payload]         [The software buffer where events will directly be written from the front end.  Note that this must be large enough to handle the size of the event.]
 * \param[in] [software_buffer]     [The maximum number of events that should be read out in one VME read. When implemented, this is typically provided as a configuration setting.]
 * \param[in] [monitoring]          [The dictionary that stores a mapping of internal processing metrics that can be monitored, when implemented in FASER.]
 * \param[in] [nevents]             [The total number of events present in the hardware buffer which you wish to read.]
 * \param[in] [nchannels_enabled]   [The total number of readout channels enabled for acquisition.]
 * \param[in] [buffer_size]         [The length of a single acquisition window in terms of the number of readings.]
 * \param[in] [readout_method]      [The read method for retrieving data from the digitizer.  Can take the values {BLT32,MBLT64,2EVME,DMA_D32FIFO,BLT32FIFO,MBLT64FIFO,2EVMEFIFO,2ESST160FIFO,2ESST267FIFO,2ESST320FIFO} each of which use different VME protocols of increasing speed.]
 * \param[in] [events_to_readout]   [The number of events to be read out by this one call to this method.]
 * \param[in] [ECRcount]            [The number of event counter resets.  This is only relevant in the case of FASER running and something that the FASER framework provides and is subsequently added to the fragment header.]
 * \param[in] [ttt_converter]       [The frequency used to convert the digitizer trigger counter to a BCID.  For LHC running, it should be 40.08 MHz but can be different in other cases.]
 * \param[in] [debug]               [A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.]

 * \note This is not actually used in the FASER DigitizerReceiverModule, which uses the reading/passing of batches of events.  But I think this is preserved for standalone running.
 * \warning [any warning if necessary]
 * \callgraph
 * \callergraph
 * \showrefby
 * \todo Clean up the function signature to remove the unused arguments.
 * \todo Can this method be removed entirely as unnecessary and deprecated?
 */
 
void vx1730::SendEventSingle(uint32_t raw_payload[], int software_buffer, std::map<std::string, float>& monitoring, int nevents, int nchannels_enabled, int buffer_size, std::string readout_method, int events_to_readout, int ECRcount, float ttt_converter, bool debug) {
  DEBUG("sendEventSingle()");
  
  // get the event
  //uint32_t raw_payload[MAXFRAGSIZE];
  this->ReadEventSingle( raw_payload, true );

  // creating the payload
  int payload_size = Payload_GetEventSize( raw_payload );
  const int total_size = sizeof(uint32_t) * payload_size;  // size of my payload in bytes

  DEBUG("PayloadSize : nwords="<<payload_size<<"  total_size="<<total_size);

  // the event ID is what should be used, in conjunction with the ECR to give a unique event tag
  // word[2] bits[23:0]
  // need to blank out the top bits because these are the channel masks
  unsigned int Header_EventCounter   = (raw_payload[2] & 0xFF000000);
  
  // the trigger time tag is used to give a "verification" of the event ID if that fails
  // it is nominally the LHC BCID but we need to do a conversion from our clock to the LHC clock
  // word[3] bits[31:0]
  unsigned int Header_TriggerTimeTag = raw_payload[3];
  
  DEBUG("Header_EventCounter   : "+to_string(Header_EventCounter));
  DEBUG("Header_TriggerTimeTag : "+to_string(Header_TriggerTimeTag));
  
  // store the faser header information
  uint8_t  local_fragment_tag = DAQFormats::EventTags::PhysicsTag;
  uint32_t local_source_id    = SourceIDs::PMTSourceID;
  uint64_t local_event_id     = (ECRcount<<24) + ((Header_EventCounter+1)&0xFFFFFF); // from the header and the ECR from sendECR() counting m_ECRcount [ECR]+[EID]
  uint16_t local_bc_id        = Header_TriggerTimeTag*(ttt_converter/125);      // trigger time tag corrected by LHCClock/TrigClock = m_ttt_converter/125, where m_ttt_converter is configurable but by default is 40.08

  // create the event fragment
  std::unique_ptr<EventFragment> fragment(new EventFragment(local_fragment_tag, local_source_id, local_event_id, local_bc_id, raw_payload, total_size ));

  // ToDo : What is the status supposed to be?
  uint16_t status=0;
  fragment->set_status( status );

  // place the raw binary event fragment on the output port
  std::unique_ptr<const byteVector> bytestream(fragment->raw());

}

/*!
 * \brief [brief description]
 *
 * [detailed description]
 *
 * \param[in] [name of input parameter] [its description]
 * \param[out] [name of output parameter] [its description]
 * \return [information about return value]
 * \sa [see also section]
 * \note [any note about the function you might have]
 * \warning [any warning if necessary]
 * \callgraph
 * \callergraph
 * \showrefby
 */
int vx1730::ReadEventSingle( uint32_t raw_payload[], bool debug ){
  DEBUG("Reading Raw Event");

  unsigned int addr;
  unsigned int data_nevents;
  unsigned int data_eventsize;
  unsigned int data_bltread;
  int return_code;

  // get the number of events
  ReadSlaveReg(m_crate, m_base_address + VX1730_EVENT_STORED, data_nevents, debug );
  ReadSlaveReg(m_crate, m_base_address + VX1730_EVENT_SIZE, data_eventsize, debug );
  ReadSlaveReg(m_crate, m_base_address + VX1730_BLT_EVENT_NB, data_bltread, debug );

  INFO("NEvents in Buffer : "<<std::dec<<data_nevents);
  INFO("Event Size        : "<<std::dec<<data_eventsize);
  INFO("BLT N Read        : "<<std::dec<<data_bltread);

  // read out event in its entirety using block read
  UINT gl_dma_buffer[MAXFRAGSIZE];
  UINT request_nof_words;
  UINT got_nof_words;

  // the size of the read will be the size of the event
  request_nof_words = data_eventsize;

  // initialize full internal buffer to 0
  for(int iWord=0; iWord<request_nof_words; iWord++) {
    gl_dma_buffer[iWord] = 0 ;
  }

  //////////////////////////////////////////
  // Block Read
  // must perform this read procedure multiple times as indicated in "Block Transfer D32/D64, 2eVME"
  //
  // each time you have to move the pointer forward as to where you put the data
  //////////////////////////////////////////
  
  int nwords_obtained=0; // this is the location in the data disk as to where you will be putting into the payload array
  while(true){
  
    // perform a single block read
    DEBUG("Reading into : "<<nwords_obtained);

    return_code = m_crate->vme_A32BLT32_read (m_base_address + 0x0000, &gl_dma_buffer[nwords_obtained], request_nof_words, &got_nof_words); 
    DEBUG("vme_A32BLT32_read:  addr = "+to_string(addr)+"   got_nof_words = "+to_string(got_nof_words)+"  return_code = "+to_string(return_code));

    // move forward the location where you will read into
    nwords_obtained += got_nof_words;
    
    // if you reach the end of the expected size of the event then break out                                                                                             
    if(nwords_obtained==request_nof_words){
      DEBUG("Break(1) nwords_obtained==request_nof_words");
      break;
    }
    
    // if you read and you get no words, then break out of loop
    if(got_nof_words==0){
      DEBUG("Break(2) got_nof_words==0");
      break;
    }
  }

  // copy the output of the BLT read to the payload object itself
  for(int iWord=0; iWord<request_nof_words; iWord++) {
    //DEBUG("Word : "<<std::setw(5)<<std::dec<<iWord<<"  0x"<<std::setfill('0')<<std::setw(8)<<std::hex<<gl_dma_buffer[iWord]);
    raw_payload[iWord]=gl_dma_buffer[iWord];
  }

  return 0;
}

/*!
 * \brief Read single raw event from digitizer
 *
 * Performs a read to acquire the data from the digitizer and store it in a software buffer for subsequent processing.
 *
 * \param[in] [raw_payload]         [The software buffer where events will directly be written from the front end.  Note that this must be large enough to handle the size of the event.]
 * \param[in] [software_buffer]     [The maximum number of events that should be read out in one VME read. When implemented, this is typically provided as a configuration setting.]
 * \param[in] [monitoring]          [The dictionary that stores a mapping of internal processing metrics that can be monitored, when implemented in FASER.]
 * \param[in] [nevents]             [The total number of events present in the hardware buffer which you wish to read.]
 * \param[in] [nchannels_enabled]   [The total number of readout channels enabled for acquisition.]
 * \param[in] [buffer_size]         [The length of a single acquisition window in terms of the number of readings.]
 * \param[in] [readout_method]      [The read method for retrieving data from the digitizer.  Can take the values {BLT32,MBLT64,2EVME,DMA_D32FIFO,BLT32FIFO,MBLT64FIFO,2EVMEFIFO,2ESST160FIFO,2ESST267FIFO,2ESST320FIFO} each of which use different VME protocols of increasing speed.]
 * \param[in] [events_to_readout]   [The number of events to be read out by this one call to this method.]
 * \param[in] [ECRcount]            [The number of event counter resets.  This is only relevant in the case of FASER running and something that the FASER framework provides and is subsequently added to the fragment header.]
 * \param[in] [ttt_converter]       [The frequency used to convert the digitizer trigger counter to a BCID.  For LHC running, it should be 40.08 MHz but can be different in other cases.]
 * \param[in] [debug]               [A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.]
 *
 * \note [any note about the function you might have]
 *
 * \callgraph
 * \callergraph
 * \showrefby
 */
void vx1730::RetrieveEventSingle(uint32_t raw_payload[], int software_buffer, std::map<std::string, float>& monitoring, int nevents, int nchannels_enabled, int buffer_size, std::string readout_method, int events_to_readout, int ECRcount, float ttt_converter, bool debug) {
  DEBUG("RetrieveEventSingle()");
  
  // get the event
  //uint32_t raw_payload[MAXFRAGSIZE];
  this->ReadEventSingle( raw_payload, true );

}

/*!
 * \brief Process single event
 *
 * Processes an event and prepares it into a FASER-style event fragment by adding the appropriate header.
 *
 * \param[in] [raw_payload]         [The software buffer where events will directly be written from the front end.  Note that this must be large enough to handle the size of the event.]
 * \param[in] [software_buffer]     [The maximum number of events that should be read out in one VME read. When implemented, this is typically provided as a configuration setting.]
 * \param[in] [monitoring]          [The dictionary that stores a mapping of internal processing metrics that can be monitored, when implemented in FASER.]
 * \param[in] [nevents]             [The total number of events present in the hardware buffer which you wish to read.]
 * \param[in] [nchannels_enabled]   [The total number of readout channels enabled for acquisition.]
 * \param[in] [buffer_size]         [The length of a single acquisition window in terms of the number of readings.]
 * \param[in] [readout_method]      [The read method for retrieving data from the digitizer.  Can take the values {BLT32,MBLT64,2EVME,DMA_D32FIFO,BLT32FIFO,MBLT64FIFO,2EVMEFIFO,2ESST160FIFO,2ESST267FIFO,2ESST320FIFO} each of which use different VME protocols of increasing speed.]
 * \param[in] [events_to_readout]   [The number of events to be read out by this one call to this method.]
 * \param[in] [ECRcount]            [The number of event counter resets.  This is only relevant in the case of FASER running and something that the FASER framework provides and is subsequently added to the fragment header.]
 * \param[in] [ttt_converter]       [The frequency used to convert the digitizer trigger counter to a BCID.  For LHC running, it should be 40.08 MHz but can be different in other cases.]
 * \param[in] [debug]               [A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.]
 *
 * \return [information about return value]
 * \sa [see also section]
 * \note [any note about the function you might have]
 * \warning [any warning if necessary]
 * \callgraph
 * \callergraph
 * \showrefby
 */
std::unique_ptr<EventFragment> vx1730::ParseEventSingle(uint32_t raw_payload[], int software_buffer, std::map<std::string, float>& monitoring, int nevents, int nchannels_enabled, int buffer_size, std::string readout_method, int events_to_readout, int ECRcount, float ttt_converter, float bcid_ttt_fix, bool debug) {
  DEBUG("ParseEventSingle()");

  // creating the payload
  int payload_size = Payload_GetEventSize( raw_payload );
  const int total_size = sizeof(uint32_t) * payload_size;  // size of my payload in bytes

  DEBUG("PayloadSize : nwords="<<payload_size<<"  total_size="<<total_size);

  // the event ID is what should be used, in conjunction with the ECR to give a unique event tag
  // word[2] bits[23:0]
  // need to blank out the top bits because these are the channel masks
  unsigned int Header_EventCounter   = (raw_payload[2] & 0xFF000000);
  
  // the trigger time tag is used to give a "verification" of the event ID if that fails
  // it is nominally the LHC BCID but we need to do a conversion from our clock to the LHC clock
  // word[3] bits[31:0]
  unsigned int Header_TriggerTimeTag = raw_payload[3];
  
  DEBUG("Header_EventCounter   : "+to_string(Header_EventCounter));
  DEBUG("Header_TriggerTimeTag : "+to_string(Header_TriggerTimeTag));
  DEBUG("ttt_converter : "<<ttt_converter);
  DEBUG("bcid_ttt_fix : "<<bcid_ttt_fix);
  
  // store the faser header information
  uint8_t  local_fragment_tag = DAQFormats::EventTags::PhysicsTag;
  uint32_t local_source_id    = SourceIDs::PMTSourceID;
  uint64_t local_event_id     = (ECRcount<<24) + ((Header_EventCounter+1)&0xFFFFFF); // from the header and the ECR from sendECR() counting m_ECRcount [ECR]+[EID]
  uint16_t local_bc_id        = (Header_TriggerTimeTag+bcid_ttt_fix)*(ttt_converter/125);      // trigger time tag corrected by LHCClock/TrigClock = m_ttt_converter/125, where m_ttt_converter is configurable but by default is 40.08

  // create the event fragment
  std::unique_ptr<EventFragment> fragment(new EventFragment(local_fragment_tag, local_source_id, local_event_id, local_bc_id, raw_payload, total_size ));

  // ToDo : What is the status supposed to be?
  uint16_t status=0;
  fragment->set_status( status );
  
  return fragment;

  //// place the raw binary event fragment on the output port
  //std::unique_ptr<const byteVector> bytestream(fragment->raw());
  //
  //return bytestream;
}

/*!
 * \brief Sends batch of events ala DAQling
 *
 * Performs reading and processing of a batch of events in a manner akin to that performed within the DAQling architecture.  This was meant to allow for standalone running and speed testing of the software outside DAQling.
 *
 * \param[in] [raw_payload]         [The software buffer where events will directly be written from the front end.  Note that this must be large enough to handle the size of the event.]
 * \param[in] [software_buffer]     [The maximum number of events that should be read out in one VME read. When implemented, this is typically provided as a configuration setting.]
 * \param[in] [monitoring]          [The dictionary that stores a mapping of internal processing metrics that can be monitored, when implemented in FASER.]
 * \param[in] [nevents]             [The total number of events present in the hardware buffer which you wish to read.]
 * \param[in] [nchannels_enabled]   [The total number of readout channels enabled for acquisition.]
 * \param[in] [buffer_size]         [The length of a single acquisition window in terms of the number of readings.]
 * \param[in] [readout_method]      [The read method for retrieving data from the digitizer.  Can take the values {BLT32,MBLT64,2EVME,DMA_D32FIFO,BLT32FIFO,MBLT64FIFO,2EVMEFIFO,2ESST160FIFO,2ESST267FIFO,2ESST320FIFO} each of which use different VME protocols of increasing speed.]
 * \param[in] [events_to_readout]   [The number of events to be read out by this one call to this method.]
 * \param[in] [ECRcount]            [The number of event counter resets.  This is only relevant in the case of FASER running and something that the FASER framework provides and is subsequently added to the fragment header.]
 * \param[in] [ttt_converter]       [The frequency used to convert the digitizer trigger counter to a BCID.  For LHC running, it should be 40.08 MHz but can be different in other cases.]
 * \param[in] [debug]               [A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.]
 *
 * \note This is not currently used within FASER and there is, instead, a similar method implemented in the DigitizerReceiverModule.
 *
 * \callgraph
 * \callergraph
 * \showrefby
 * \todo Figure out how to get this to compile with the #ifdef at the end so that it can be compiled and used standalone.
 */
void vx1730::SendEventBatch(uint32_t raw_payload[], int software_buffer, std::map<std::string, float>& monitoring, int nevents, int nchannels_enabled, int buffer_size, std::string readout_method, int events_to_readout, int ECRcount, float ttt_converter, bool debug) {
  // system time before
  auto start_header_time = chrono::high_resolution_clock::now(); 
  
  std::memset(raw_payload, 0, software_buffer);
  
  // can predict the size of the buffer that will be read out
  // [1] number of events in buffer
  // [2] known event size (nchannels enabled, buffer size)
    
  DEBUG("nevents              : "<<std::dec<<nevents);
  DEBUG("nchannels_enabled    : "<<std::dec<<nchannels_enabled);
  DEBUG("buffer_size          : "<<std::dec<<buffer_size);
  DEBUG("readout_method       : "<<std::dec<<readout_method);
  DEBUG("events_to_readout    : "<<std::dec<<events_to_readout);

  int event_size = (nchannels_enabled*(buffer_size/2.0) + 4);  
  
  // get the full event buffer from the digitizer board
  //uint32_t raw_payload[MAXFRAGSIZE];
  int nwords=-1;
  DEBUG("Prepping to read buffer");
  
  int nevents_to_transfer = events_to_readout;
  
  if(nevents_to_transfer>nevents)
    nevents_to_transfer = nevents;
    
    
  // system time before
  auto end_header_time = chrono::high_resolution_clock::now(); 
  float time_header_time = chrono::duration_cast<chrono::nanoseconds>(end_header_time - start_header_time).count() * 1e-9; 
  DEBUG("Time taken by header is : " << fixed << time_header_time << setprecision(5) << " sec ");
    

  // system time before
  auto start_read_time = chrono::high_resolution_clock::now(); 

  nwords = this->ReadEventBatch(raw_payload, software_buffer, monitoring, nevents, nchannels_enabled, buffer_size, readout_method, nevents_to_transfer);

  // system time before
  auto end_read_time = chrono::high_resolution_clock::now(); 
  float time_read_time = chrono::duration_cast<chrono::nanoseconds>(end_read_time - start_read_time).count() * 1e-9; 
  DEBUG("Time taken by buffer_readout is : " << fixed << time_read_time << setprecision(5) << " sec ");
  
  
  // system time before
  auto start_filler_time = chrono::high_resolution_clock::now(); 

  DEBUG("Buffer has been read");
  
  DEBUG("Monitoring size : "<<monitoring.size());
  std::map<std::string, float>::iterator it;
  for (it = monitoring.begin(); it != monitoring.end(); it++) {
    DEBUG(" - "<<it->first << "   :   " << it->second);
  }
  
  int nevents_after     = this->DumpEventCount();
  
  DEBUG(">>>>>>> NEv Before      : "<<std::dec<<nevents);
  DEBUG(">>>>>>> NEv After       : "<<std::dec<<nevents_after);
  DEBUG(">>>>>>> NEv Requested   : "<<std::dec<<nevents_to_transfer);
  
  int nwords_expected = nevents_to_transfer *  event_size;
  
  // these don't necessarily have to line up 
  // perhaps an event appeared just before reading
  // trust the words you obtain and divide by evnet size to know the number
  // of events you will have to parse
  DEBUG("Words expected : "<<nwords_expected);
  DEBUG("Words obtained : "<<nwords);
  
  int nevents_obtained = nwords/event_size;
  
  // ToDo : implement check to make sure it divides
   
  uint32_t single_event_raw_payload[10000];
  int eventLocation=0;
  
  // system time before
  auto end_filler_time = chrono::high_resolution_clock::now(); 
  float time_filler_time = chrono::duration_cast<chrono::nanoseconds>(end_filler_time - start_filler_time).count() * 1e-9; 
  DEBUG("Time taken by buffer_fillerout is : " << fixed << time_filler_time << setprecision(5) << " sec ");
  
  // system time before
  auto start_parse_time = chrono::high_resolution_clock::now(); 
  
  DEBUG("Sending NEvents : "<<nevents_obtained);
  
  // initialize variables only once
  unsigned int Header_TriggerTimeTag = 0;
  
  uint8_t  local_fragment_tag = 0;
  uint32_t local_source_id    = 0;
  uint64_t local_event_id     = 0;
  uint16_t local_bc_id        = 0;
  uint16_t status             = 0;

  int payload_size = 0;
  
  unsigned int Header_EventCounter = 0;
  
  int total_size = 0;

  // perform transfer for every event that was read out in the buffer
  for(int iev=0; iev<nevents_obtained; iev++){
  
    DEBUG("Moving event : "<<iev<<"  "<<eventLocation<<"  "<<event_size);
  
    // saves one event starting in eventLocation to single_event_raw_payload
    DEBUG("Retrieve event : location="<<eventLocation<<"  size="<<event_size);
    eventLocation = GetSingleEvent(raw_payload, single_event_raw_payload, eventLocation, event_size);
  
  
    if(debug){
      // to check the eventCounter to be sure
      DEBUG("header[0] : "<<ConvertIntToWord(single_event_raw_payload[0]));
      DEBUG("header[1] : "<<ConvertIntToWord(single_event_raw_payload[1]));
      DEBUG("header[2] : "<<ConvertIntToWord(single_event_raw_payload[2]));
      DEBUG("header[3] : "<<ConvertIntToWord(single_event_raw_payload[3]));
    
      int nsampwords = ((event_size-4)/nchannels_enabled);
      int print_chan = 6;
    
      for(int idat=0; idat<nsampwords; idat++){
        DEBUG("data["<<(idat*2)  <<"]   : "<<std::dec<< ((single_event_raw_payload[4+(nsampwords*print_chan)+idat])& 0x0000FFFF) );
        DEBUG("data["<<(idat*2)+1<<"]   : "<<std::dec<< ((single_event_raw_payload[4+(nsampwords*print_chan)+idat] & 0xFFFF0000)>>16) );
      }
    }
    
    // do the following for each of the events that has been parsed
    // creating the payload
    //int payload_size = Payload_GetEventSize( single_event_raw_payload );
    payload_size = raw_payload[0] & 0x0FFFFFF;
    total_size = sizeof(uint32_t) * payload_size;  // size of my payload in bytes

    DEBUG("PayloadSize : nwords="<<payload_size<<"  total_size="<<total_size);

    // the event ID is what should be used, in conjunction with the ECR to give a unique event tag
    // word[2] bits[23:0]
    // need to blank out the top bits because these are the channel masks
    Header_EventCounter   = (single_event_raw_payload[2] & 0xFF000000);
  
    // the trigger time tag is used to give a "verification" of the event ID if that fails
    // it is nominally the LHC BCID but we need to do a conversion from our clock to the LHC clock
    // word[3] bits[31:0]
    Header_TriggerTimeTag = single_event_raw_payload[3];
  
    DEBUG("Header_EventCounter   : "+to_string(Header_EventCounter));
    DEBUG("Header_TriggerTimeTag : "+to_string(Header_TriggerTimeTag));
  
    // store the faser header information
    local_fragment_tag = DAQFormats::EventTags::PhysicsTag;
    local_source_id    = SourceIDs::PMTSourceID;
    local_event_id     = (ECRcount<<24) + ((Header_EventCounter+1)&0xFFFFFF); // from the header and the ECR from sendECR() counting m_ECRcount [ECR]+[EID]
    local_bc_id        = Header_TriggerTimeTag*(ttt_converter/125);      // trigger time tag corrected by LHCClock/TrigClock = m_ttt_converter/125, where m_ttt_converter is configurable but by default is 40.08
 
    // create the event fragment
    std::unique_ptr<EventFragment> fragment(new EventFragment(local_fragment_tag, local_source_id, local_event_id, local_bc_id, single_event_raw_payload, total_size ));

    // ToDo : What is the status supposed to be?
    status=0;
    fragment->set_status( status );

    // place the raw binary event fragment on the output port
    std::unique_ptr<const byteVector> bytestream(fragment->raw());
    
//#ifndef LOCAL
//    // only necessary for the real module
//    daqling::utilities::Binary binData(bytestream->data(),bytestream->size());
//    m_connections.put(0, binData); 
//#endif  
  
  } 
  
  // system time after
  auto end_parse_time = chrono::high_resolution_clock::now(); 
  
  float time_parse_time = chrono::duration_cast<chrono::nanoseconds>(end_parse_time - start_parse_time).count() * 1e-9; 
  DEBUG("Time taken by event parsing is : " << fixed << time_parse_time << setprecision(5) << " sec ");
    
  monitoring["time_header_time"]   = time_header_time;  
  monitoring["time_filler_time"]   = time_filler_time;  
  monitoring["time_read_time"]     = time_read_time;
  monitoring["time_parse_time"]    = time_parse_time;

}

/*!
 * \brief Gets a sequence of events from the digitizer
 *
 * Prepares the context for the block read of events and performs the read of a batch of events.
 *
 * \param[in] [raw_payload]         [The software buffer where events will directly be written from the front end.  Note that this must be large enough to handle the size of the event.]
 * \param[in] [software_buffer]     [The maximum number of events that should be read out in one VME read. When implemented, this is typically provided as a configuration setting.]
 * \param[in] [monitoring]          [The dictionary that stores a mapping of internal processing metrics that can be monitored, when implemented in FASER.]
 * \param[in] [nevents]             [The total number of events present in the hardware buffer which you wish to read.]
 * \param[in] [nchannels_enabled]   [The total number of readout channels enabled for acquisition.]
 * \param[in] [buffer_size]         [The length of a single acquisition window in terms of the number of readings.]
 * \param[in] [readout_method]      [The read method for retrieving data from the digitizer.  Can take the values {BLT32,MBLT64,2EVME,DMA_D32FIFO,BLT32FIFO,MBLT64FIFO,2EVMEFIFO,2ESST160FIFO,2ESST267FIFO,2ESST320FIFO} each of which use different VME protocols of increasing speed.]
 * \param[in] [events_to_readout]   [The number of events to be read out by this one call to this method.]
 * \param[in] [ECRcount]            [The number of event counter resets.  This is only relevant in the case of FASER running and something that the FASER framework provides and is subsequently added to the fragment header.]
 * \param[in] [ttt_converter]       [The frequency used to convert the digitizer trigger counter to a BCID.  For LHC running, it should be 40.08 MHz but can be different in other cases.]
 * \param[in] [debug]               [A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.]
 * \return The number of events that were obtained in the block read

 * \note This is a core method which interacts most often with the hardware during running of FASER.  
 *
 * \callgraph
 * \callergraph
 * \showrefby
 * \todo Use a smart pointer instead of a C-style array that is dynamically allocated
 */
int vx1730::RetrieveEventBatch(uint32_t raw_payload[], int software_buffer, std::map<std::string, float>& monitoring, int nevents, int nchannels_enabled, int buffer_size, std::string readout_method, int events_to_readout, int ECRcount, float ttt_converter, bool debug) {
  DEBUG("RetrieveEventBatch()");
  
  // system time before
  auto start_header_time = chrono::high_resolution_clock::now(); 
  
  // clear the data in the buffer payload
  std::memset(raw_payload, 0, software_buffer);
  
  // can predict the size of the buffer that will be read out
  // [1] number of events in buffer
  // [2] known event size (nchannels enabled, buffer size)
    
  DEBUG("nevents              : "<<std::dec<<nevents);
  DEBUG("nchannels_enabled    : "<<std::dec<<nchannels_enabled);
  DEBUG("buffer_size          : "<<std::dec<<buffer_size);
  DEBUG("readout_method       : "<<std::dec<<readout_method);
  DEBUG("events_to_readout    : "<<std::dec<<events_to_readout);

  int event_size = (nchannels_enabled*(buffer_size/2.0) + 4);  
  
  // get the full event buffer from the digitizer board
  //uint32_t raw_payload[MAXFRAGSIZE];
  int nwords=-1;
  DEBUG("Prepping to read buffer");
  
  int nevents_to_transfer = events_to_readout;
  
  if(nevents_to_transfer>nevents)
    nevents_to_transfer = nevents;
    
    
  // system time before
  auto end_header_time = chrono::high_resolution_clock::now(); 
  float time_header_time = chrono::duration_cast<chrono::nanoseconds>(end_header_time - start_header_time).count() * 1e-9; 
  DEBUG("Time taken by header is : " << fixed << time_header_time << setprecision(5) << " sec ");
    

  // system time before
  auto start_read_time = chrono::high_resolution_clock::now(); 

  nwords = this->ReadEventBatch(raw_payload, software_buffer, monitoring, nevents, nchannels_enabled, buffer_size, readout_method, nevents_to_transfer);

  // system time before
  auto end_read_time = chrono::high_resolution_clock::now(); 
  float time_read_time = chrono::duration_cast<chrono::nanoseconds>(end_read_time - start_read_time).count() * 1e-9; 
  DEBUG("Time taken by buffer_readout is : " << fixed << time_read_time << setprecision(5) << " sec ");
  
  
  // system time before
  auto start_filler_time = chrono::high_resolution_clock::now(); 

  DEBUG("Buffer has been read");
  
  DEBUG("Monitoring size : "<<monitoring.size());
  std::map<std::string, float>::iterator it;
  for (it = monitoring.begin(); it != monitoring.end(); it++) {
    DEBUG(" - "<<it->first << "   :   " << it->second);
  }
  
  int nevents_after     = this->DumpEventCount();
  
  DEBUG(">>>>>>> NEv Before      : "<<std::dec<<nevents);
  DEBUG(">>>>>>> NEv After       : "<<std::dec<<nevents_after);
  DEBUG(">>>>>>> NEv Requested   : "<<std::dec<<nevents_to_transfer);
  
  int nwords_expected = nevents_to_transfer *  event_size;
  
  // these don't necessarily have to line up 
  // perhaps an event appeared just before reading
  // trust the words you obtain and divide by evnet size to know the number
  // of events you will have to parse
  DEBUG("Words expected : "<<nwords_expected);
  DEBUG("Words obtained : "<<nwords);
  
  int nevents_obtained = nwords/event_size;
  
  // ToDo : implement check to make sure it divides
   
  uint32_t single_event_raw_payload[10000];
  int eventLocation=0;
  
  // system time before
  auto end_filler_time = chrono::high_resolution_clock::now(); 
  float time_filler_time = chrono::duration_cast<chrono::nanoseconds>(end_filler_time - start_filler_time).count() * 1e-9; 
  DEBUG("Time taken by buffer_fillerout is : " << fixed << time_filler_time << setprecision(5) << " sec ");
  
  // system time before
  auto start_parse_time = chrono::high_resolution_clock::now(); 
  
  DEBUG("Sending NEvents : "<<nevents_obtained);
  
  monitoring["time_header_time"]   = time_header_time;  
  monitoring["time_filler_time"]   = time_filler_time;  
  monitoring["time_read_time"]     = time_read_time;
  
  return nevents_obtained;
}

/*!
 * \brief Performs read of event batch from VME to software
 *
 * Performs a block read of multiple events from the digitizer and stores them in the software buffer for subsequent processing elsewhere.  
 *
 * \param[in] [raw_payload]         [The software buffer where events will directly be written from the front end.  Note that this must be large enough to handle the size of the event.]
 * \param[in] [software_buffer]     [The maximum number of events that should be read out in one VME read. When implemented, this is typically provided as a configuration setting.]
 * \param[in] [monitoring]          [The dictionary that stores a mapping of internal processing metrics that can be monitored, when implemented in FASER.]
 * \param[in] [nevents]             [The total number of events present in the hardware buffer which you wish to read.]
 * \param[in] [nchannels_enabled]   [The total number of readout channels enabled for acquisition.]
 * \param[in] [buffer_size]         [The length of a single acquisition window in terms of the number of readings.]
 * \param[in] [readout_method]      [The read method for retrieving data from the digitizer.  Can take the values {BLT32,MBLT64,2EVME,DMA_D32FIFO,BLT32FIFO,MBLT64FIFO,2EVMEFIFO,2ESST160FIFO,2ESST267FIFO,2ESST320FIFO} each of which use different VME protocols of increasing speed.]
 * \param[in] [events_to_readout]   [The number of events to be read out by this one call to this method.]
 * \param[in] [ECRcount]            [The number of event counter resets.  This is only relevant in the case of FASER running and something that the FASER framework provides and is subsequently added to the fragment header.]
 * \param[in] [ttt_converter]       [The frequency used to convert the digitizer trigger counter to a BCID.  For LHC running, it should be 40.08 MHz but can be different in other cases.]
 * \param[in] [debug]               [A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.]
 * \return The total number of 32 bit words obtained by the read.

 * \note This is the core method which interacts most often with the hardware during running of FASER.  
 *
 * \callgraph
 * \callergraph
 * \showrefby
 * \todo Use a smart pointer instead of a C-style array that is dynamically allocated
 */
int vx1730::ReadEventBatch( uint32_t raw_payload[], int software_buffer, std::map<std::string, float>& monitoring, int nevents_in_buffer, int nchannels_enabled, int buffer_size, const std::string& readout_method, int nevents_to_transfer, bool debug ){
  DEBUG("vx1730::ReadEventBatch");
  
  // system time before
  auto start_buffer_func = chrono::high_resolution_clock::now(); 
  
  // read out event in its entirety using block read
  uint32_t request_nof_words;
  uint32_t got_nof_words;

  unsigned int addr;
  unsigned int data_eventsize;
  int return_code;

  // get the number of events
  //ReadSlaveReg(m_crate, m_base_address + VX1730_EVENT_SIZE, data_eventsize, debug );
  
  data_eventsize = ((buffer_size/2)*nchannels_enabled)+4;

  // to be able to toggle the the number of events that are read from buffer
  int transfer_words = data_eventsize * nevents_to_transfer;

  // the size of the read will be the full buffer
  int full_buffer_size = data_eventsize*nevents_in_buffer;

  // the requested number of words will be the BLT configured amount
  // ensures we don't overflow the software buffer which is fixed size
  request_nof_words = transfer_words;

  DEBUG("Event Size             : "<<std::dec<<data_eventsize);  
  DEBUG("NEvents in Full Buffer : "<<std::dec<<nevents_in_buffer);
  DEBUG("NChan enabled          : "<<std::dec<<nchannels_enabled);
  DEBUG("Buffer size(manual)    : "<<std::dec<<buffer_size);
  DEBUG("Readout Method         : "<<std::dec<<readout_method);
  DEBUG("NEvents to be Readout  : "<<std::dec<<nevents_to_transfer);  
  DEBUG("SoftwareBufferSize     : "<<software_buffer);
  DEBUG("HWBufferContent        : "<<full_buffer_size);
  DEBUG("request_nof_words      : "<<request_nof_words);
  
  // if you don't need to transfer the full amount don't try to transfer so much
  if(request_nof_words>full_buffer_size){
    request_nof_words=full_buffer_size;
  }
  
  DEBUG(">>> NEW request_nof_words(full_buffer_size)  : "<<request_nof_words);
  
  if(request_nof_words>software_buffer){
    request_nof_words=software_buffer;
  }
  
  // fix for odd number of words with BLT 64bit reads
  // if you read too many words then it corrupts the digitizer fully and requires a manual
  // reset of the system
  int leftover_words = request_nof_words%4;

  if(leftover_words!=0){
    DEBUG("Asking to read odd number of words : "<<request_nof_words);
    DEBUG("Leftover : "<<leftover_words);
    request_nof_words -= leftover_words;
  }

  DEBUG(">>> NEW request_nof_words(software_buffer)       : "<<request_nof_words);  

  // initialize full internal buffer to 0
  DEBUG("Initializing software buffer to 0");
  
  // set the event batch data back to 0
  //std::memset(raw_payload, 0, software_buffer);

  //////////////////////////////////////////
  // Block Read
  //  - must perform this read procedure multiple times as indicated in "Block Transfer D32/D64, 2eVME"
  //  - each time you have to move the pointer forward as to where you put the data
  //////////////////////////////////////////
  DEBUG("Performing hardware BLT read to software buffer");

  // this is the location in the software buffer where you will be putting into the payload array
  int nwords_obtained=0; 

  // number of reads taken to flush the desired buffer contents
  int nreads=0;
  
  // system time before
  auto start_block_read = chrono::high_resolution_clock::now(); 
  
  // loop that extracts the data from the board using the configurable readout_method type of access
  while(true){
  
    // perform a single block read
    DEBUG("Reading into : "<<nwords_obtained);

    // toggle of *working* read methods
    unsigned int data_address = m_base_address + 0x0000;
    
    if(readout_method=="BLT32"){               return_code = m_crate->vme_A32BLT32_read (data_address, &raw_payload[nwords_obtained], request_nof_words, &got_nof_words);         /* 300 Hz */ } 
    else if(readout_method=="MBLT64"){         return_code = m_crate->vme_A32MBLT64_read (data_address, &raw_payload[nwords_obtained], request_nof_words, &got_nof_words);        /* 440 Hz */ }
    else if(readout_method=="2EVME"){          return_code = m_crate->vme_A32_2EVME_read (data_address, &raw_payload[nwords_obtained], request_nof_words, &got_nof_words);        /* 500 Hz */ }
    else if(readout_method=="DMA_D32FIFO"){    return_code = m_crate->vme_A32DMA_D32FIFO_read (data_address, &raw_payload[nwords_obtained], request_nof_words, &got_nof_words);   /*        */ }
    else if(readout_method=="BLT32FIFO"){      return_code = m_crate->vme_A32BLT32FIFO_read (data_address, &raw_payload[nwords_obtained], request_nof_words, &got_nof_words);     /* 360 Hz */ }
    else if(readout_method=="MBLT64FIFO"){     return_code = m_crate->vme_A32MBLT64FIFO_read (data_address, &raw_payload[nwords_obtained], request_nof_words, &got_nof_words);    /* 600 Hz */ }
    else if(readout_method=="2EVMEFIFO"){      return_code = m_crate->vme_A32_2EVMEFIFO_read (data_address, &raw_payload[nwords_obtained], request_nof_words, &got_nof_words);    /* 660 Hz */ }
    else if(readout_method=="2ESST160FIFO"){   return_code = m_crate->vme_A32_2ESST160FIFO_read (data_address, &raw_payload[nwords_obtained], request_nof_words, &got_nof_words); /* 710 Hz */ }
    else if(readout_method=="2ESST267FIFO"){   return_code = m_crate->vme_A32_2ESST267FIFO_read (data_address, &raw_payload[nwords_obtained], request_nof_words, &got_nof_words); /* 710 Hz */ }
    else if(readout_method=="2ESST320FIFO"){   return_code = m_crate->vme_A32_2ESST320FIFO_read (data_address, &raw_payload[nwords_obtained], request_nof_words, &got_nof_words); /* 710 Hz */ }
    else{
      ERROR("Not a valid VME readout method - nothing has been read");
    }
    
    DEBUG("Single Read :  addr = "+to_string(data_address)+"   got_nof_words = "+to_string(got_nof_words)+"  return_code = "+to_string(return_code));    
    
    // count the number of reads which have been performed
    nreads++;
    
    // move forward the location where you will read into
    nwords_obtained += got_nof_words;
    
    // if you read and you get no words, then break out of loop
    if(got_nof_words==0){
      DEBUG("Break : got_nof_words==0");
      break;
    }
    
    // if you reach the number of events that is imposed
    if(nwords_obtained>=request_nof_words){
      DEBUG("Break : nwords_obtained>=request_nof_words");
      break;
    }
    
  }

  auto end_block_read = chrono::high_resolution_clock::now();

  auto start_leftover_read = chrono::high_resolution_clock::now();

  // read the final word if it was previously an odd number
  if(leftover_words!=0){
    for(int iread=0; iread<leftover_words; iread++){
      DEBUG("Single Read for odd range : "<<iread);
      // toggle of *working* read methods
      unsigned int data_address = m_base_address + 0x0000;
      
      // ToDo : could this be sped up with BLT read
      // in general this will not reduce the number of reads by much
      // but could be an optimization for speed
      return_code = m_crate->vme_A32D32_read (data_address, &raw_payload[nwords_obtained]);
      nwords_obtained++;
    }
  }
  
  // system time after
  auto end_leftover_read = chrono::high_resolution_clock::now();
  
  // difference
  float time_taken_block_readout = chrono::duration_cast<chrono::nanoseconds>(end_block_read - start_block_read).count() * 1e-9; 
  DEBUG("Time taken by block_readout is : " << fixed << time_taken_block_readout << setprecision(5) << " sec ");

  float time_taken_leftover_readout = chrono::duration_cast<chrono::nanoseconds>(end_leftover_read - start_leftover_read).count() * 1e-9; 
  DEBUG("Time taken by leftover_readout is : " << fixed << time_taken_leftover_readout << setprecision(5) << " sec ");

  DEBUG("NReads : "<<nreads);
    
  
  DEBUG("DONE check");
  
  // system time before
  auto end_buffer_func = chrono::high_resolution_clock::now(); 

  // difference
  float time_taken_buffer_func = chrono::duration_cast<chrono::nanoseconds>(end_buffer_func - start_buffer_func).count() * 1e-9; 
  DEBUG("Time taken by buffer_function is : " << fixed << time_taken_buffer_func << setprecision(5) << " sec ");

  monitoring["block_readout_time"] = time_taken_block_readout;
  monitoring["leftover_readout_time"] = time_taken_leftover_readout;
  monitoring["nreads_for_buffer"] = nreads;        
  monitoring["buffer_func_time"] = time_taken_buffer_func;
  
  return nwords_obtained;
}

/*!
 * \brief Parses a batch of events from the raw VME read
 *
 * Splits up the raw payload from the digitizer, adds the appropriate FASER fragment header to each, and packages them into a vector of FASER event fragments for transmission within faser/daq.
 *
 * \param[in] [raw_payload]         [The software buffer where events will directly be written from the front end.  Note that this must be large enough to handle the size of the event.]
 * \param[in] [software_buffer]     [The maximum number of events that should be read out in one VME read. When implemented, this is typically provided as a configuration setting.]
 * \param[in] [monitoring]          [The dictionary that stores a mapping of internal processing metrics that can be monitored, when implemented in FASER.]
 * \param[in] [nevents]             [The total number of events present in the hardware buffer which you wish to read.]
 * \param[in] [nchannels_enabled]   [The total number of readout channels enabled for acquisition.]
 * \param[in] [buffer_size]         [The length of a single acquisition window in terms of the number of readings.]
 * \param[in] [readout_method]      [The read method for retrieving data from the digitizer.  Can take the values {BLT32,MBLT64,2EVME,DMA_D32FIFO,BLT32FIFO,MBLT64FIFO,2EVMEFIFO,2ESST160FIFO,2ESST267FIFO,2ESST320FIFO} each of which use different VME protocols of increasing speed.]
 * \param[in] [events_to_readout]   [The number of events to be read out by this one call to this method.]
 * \param[in] [ECRcount]            [The number of event counter resets.  This is only relevant in the case of FASER running and something that the FASER framework provides and is subsequently added to the fragment header.]
 * \param[in] [ttt_converter]       [The frequency used to convert the digitizer trigger counter to a BCID.  For LHC running, it should be 40.08 MHz but can be different in other cases.]
 * \param[in] [debug]               [A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.]
 * \return A vector of FASER-style EventFragments from the digitizer.
 *
 * \note This is one of the core methods used in FASER running.

 * \callgraph
 * \callergraph
 * \showrefby
 * \todo Use a smart pointer instead of a C-style array that is dynamically allocated
 */
std::vector<EventFragment> vx1730::ParseEventBatch(uint32_t raw_payload[], int software_buffer, int nevents_obtained, std::map<std::string, float>& monitoring, int nevents, int nchannels_enabled, int buffer_size, std::string readout_method, int events_to_readout, int ECRcount, float ttt_converter, float bcid_ttt_fix, bool debug) {
  DEBUG("Parsing event batch");
  
  // system time after
  auto start_parse_time = chrono::high_resolution_clock::now(); 
  
  
  // ToDo : implement check to make sure it divides
  //  uint32_t single_event_raw_payload[10000];

  unsigned int* single_event_raw_payload = NULL;
  single_event_raw_payload = new unsigned int[10000];

  int eventLocation=0;
  
  int event_size = (nchannels_enabled*(buffer_size/2.0) + 4);  
  
  DEBUG("Sending NEvents : "<<nevents_obtained);
  
  // initialize variables only once
  unsigned int Header_TriggerTimeTag = 0;
  
  uint8_t  local_fragment_tag = 0;
  uint32_t local_source_id    = 0;
  uint64_t local_event_id     = 0;
  uint16_t local_bc_id        = 0;
  uint16_t status             = 0;

  int payload_size = 0;
  
  unsigned int Header_EventCounter = 0;
  
  int total_size = 0;
  
  std::vector<EventFragment> all_fragments;

  // perform transfer for every event that was read out in the buffer
  for(int iev=0; iev<nevents_obtained; iev++){
    if(debug) DEBUG("Moving event : "<<iev<<"  "<<eventLocation<<"  "<<event_size);
  
    // saves one event starting in eventLocation to single_event_raw_payload
    if(debug) DEBUG("Retrieve event : location="<<eventLocation<<"  size="<<event_size);
    eventLocation = GetSingleEvent(raw_payload, single_event_raw_payload, eventLocation, event_size);
   
    // print the header for debugging purposes
    if(debug) DEBUG("header[0] : "<<ConvertIntToWord(single_event_raw_payload[0]));
    if(debug) DEBUG("header[1] : "<<ConvertIntToWord(single_event_raw_payload[1]));
    if(debug) DEBUG("header[2] : "<<ConvertIntToWord(single_event_raw_payload[2]));
    if(debug) DEBUG("header[3] : "<<ConvertIntToWord(single_event_raw_payload[3]));
    
    int nsampwords = ((event_size-4)/nchannels_enabled);
    int print_chan = 1;
    
    //for(int idat=0; idat<nsampwords; idat++){
    //  DEBUG("data["<<(idat*2)  <<"]   : "<<std::dec<< ((single_event_raw_payload[4+(nsampwords*print_chan)+idat])& 0x0000FFFF) );
    //  DEBUG("data["<<(idat*2)+1<<"]   : "<<std::dec<< ((single_event_raw_payload[4+(nsampwords*print_chan)+idat] & 0xFFFF0000)>>16) );
    //}
    
    // do the following for each of the events that has been parsed
    // creating the payload
    //int payload_size = Payload_GetEventSize( single_event_raw_payload );
    payload_size = raw_payload[0] & 0x0FFFFFF;
    total_size = sizeof(uint32_t) * payload_size;  // size of my payload in bytes

    if(debug) DEBUG("PayloadSize : nwords="<<payload_size<<"  total_size="<<total_size);

    // the event ID is what should be used, in conjunction with the ECR to give a unique event tag
    // word[2] bits[23:0]
    // need to blank out the top bits because these are the channel masks
    Header_EventCounter   = (single_event_raw_payload[2] & 0x00FFFFFF);
  
    // the trigger time tag is used to give a "verification" of the event ID if that fails
    // it is nominally the LHC BCID but we need to do a conversion from our clock to the LHC clock
    // word[3] bits[31:0]
    Header_TriggerTimeTag = single_event_raw_payload[3];
  
    if(debug) DEBUG("Header_EventCounter   : "+to_string(Header_EventCounter));
    if(debug) DEBUG("Header_TriggerTimeTag : "+to_string(Header_TriggerTimeTag));
    if(debug) DEBUG("ttt_converter : "<<ttt_converter);
    if(debug) DEBUG("bcid_ttt_fix : "<<bcid_ttt_fix);
  
    // store the faser header information
    local_fragment_tag = EventTags::PhysicsTag;
    local_source_id    = SourceIDs::PMTSourceID;
    local_event_id     = (ECRcount<<24) + ((Header_EventCounter+1)&0xFFFFFF); // from the header and the ECR from sendECR() counting m_ECRcount [ECR]+[EID]
    local_bc_id        = (Header_TriggerTimeTag+bcid_ttt_fix)*(ttt_converter/125.0);      // trigger time tag corrected by LHCClock/TrigClock = m_ttt_converter/125, where m_ttt_converter is configurable but by default is 40.08

    if(debug) DEBUG("FASERHeader - fragment_tag : "<<std::hex<<(uint32_t)local_fragment_tag);
    if(debug) DEBUG("FASERHeader - source_id    : "<<std::hex<<local_source_id   );
    if(debug) DEBUG("FASERHeader - event_id     : "<<std::dec<<local_event_id    );
    if(debug) DEBUG("FASERHeader - bc_id        : "<<std::dec<<local_bc_id       );
 
    // create the event fragment
    //std::unique_ptr<EventFragment> fragment(new EventFragment(local_fragment_tag, local_source_id, local_event_id, local_bc_id, single_event_raw_payload, total_size ));
    EventFragment fragment = EventFragment(local_fragment_tag, local_source_id, local_event_id, local_bc_id, single_event_raw_payload, total_size );

    // ToDo : What is the status supposed to be?
    status=0;
    fragment.set_status( status );
    
    // add to vector for return
    all_fragments.push_back(fragment);
  } 
  
  // system time after
  auto end_parse_time = chrono::high_resolution_clock::now(); 

  float time_parse_time = chrono::duration_cast<chrono::nanoseconds>(end_parse_time - start_parse_time).count() * 1e-9; 
  DEBUG("Time taken by event parsing is : " << fixed << time_parse_time << setprecision(5) << " sec ");

  monitoring["time_parse_time"]    = time_parse_time;

  // deallocate dynamic memory
  delete [] single_event_raw_payload;
  

  return all_fragments;
}

/*!
 * \brief Copy one event from the raw payload
 *
 * Retrieves the front event from the sequence of words obtained during a block read which themselves are a sequence of events
 *
 * \param[in] [raw_payload] [The payload containing a sequence of events]
 * \param[in] [single_event_raw_payload] [The place where you are going to put the single first event]
 * \param[in] [eventLocation] [The location within the sequence of events where the single event you want to pick resides]
 * \param[in] [eventSize] [The length of the event]
 *
 * \return [The updated location of the "front event"]
 *
 * \callgraph
 * \callergraph
 * \showrefby
 * \todo Use smart pointers instead of C-style memory everywhere
 */
int vx1730::GetSingleEvent( uint32_t raw_payload[], uint32_t single_event_raw_payload[], int eventLocation, int eventSize){
  std::copy(raw_payload+eventLocation, raw_payload+eventLocation+eventSize, single_event_raw_payload);
  return eventLocation+eventSize;
}

/*!
 * \brief Get number of events stored in digitizer hardware buffer
 *
 * Get number of events stored in digitizer hardware buffer
 *
 * \param[in] [debug]  [A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.]
 * \return [The number of events present in the hardware buffer]
 * \callgraph
 * \callergraph
 * \showrefby
 */
int vx1730::DumpEventCount( bool debug ){
  //DEBUG("Getting current number of events in the buffer");

  unsigned int n_events_in_buffer;
  ReadSlaveReg(m_crate, m_base_address + VX1730_EVENT_STORED, n_events_in_buffer, debug );

  //unsigned int ev_size;
  //ReadSlaveReg(m_crate, m_base_address + VX1730_EVENT_SIZE, ev_size, debug );
//
  //unsigned int vme_status;
  //ReadSlaveReg(m_crate, m_base_address + VX1730_VME_STATUS, vme_status, debug );
//
  //unsigned int acquisition_status;
  //ReadSlaveReg(m_crate, m_base_address + VX1730_ACQUISITION_STATUS, acquisition_status, debug );
  
  //DEBUG("EVCount: ");
  //DEBUG("n_events_in_buffer: "<<std::dec<<n_events_in_buffer);
  //DEBUG("ev_size: "<<std::dec<<ev_size);
  //DEBUG("vme_status: "<<std::hex<<vme_status);
  //DEBUG("acquisition_status: "<<std::hex<<acquisition_status);

  return n_events_in_buffer;
}

/*!
 * \brief Send software trigger
 *
 * Sends a software acquisition trigger. 
 *
 * \param[in] [debug]  [A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.]
 * \note This will only actually cause an acquisition of data if in the configuration, you have enabled the data to be acquired with software triggers.
 * \callgraph
 * \callergraph
 * \showrefby
 */
void vx1730::SendSWTrigger( bool debug ){
  DEBUG("Sending software trigger");
  WriteSlaveReg(m_crate, m_base_address + VX1730_SW_TRIGGER, 0x1, debug );
}

/*!
 * \brief Read buffer length from digitizer
 *
 * Reads the configured value of the buffer length
 *
 * \warning Because this is performing VME reads, and the buffer length should be configured initially, its probably best to not use this unless its absolutely necessary for some reason.  Rather, keep track of your configured buffer length.
 * \callgraph
 * \callergraph
 * \showrefby
 */
int vx1730::RetrieveBufferLength(){
  DEBUG("vx1730::RetrieveBufferLength()");

  int n_samples_per_buffer = 0;
  
  unsigned int data;

  // global information
  DEBUG("Before custom size check");
  ReadSlaveReg(m_crate, m_base_address+VX1730_CUSTOM_SIZE, data);
  DEBUG("After custom size check");
  if(data==0x0){
    ReadSlaveReg(m_crate, m_base_address+VX1730_BUFFER_ORGANIZATION, data);
    INFO("BUFFER_ORGANIZATION : Fixed Length");
    INFO("BUFFER_ORGANIZATION  ("<<std::hex<<VX1730_BUFFER_ORGANIZATION<<") [hex]: 0x"<<std::hex<<data<<"  -  nsamples="<<std::dec<<GetBufferLength(data));
  
    n_samples_per_buffer = GetBufferLength(data);
  }
  else{
    ReadSlaveReg(m_crate, m_base_address+VX1730_CUSTOM_SIZE, data);
    INFO("BUFFER_ORGANIZATION : Custom Length");
    INFO("BUFFER_ORGANIZATION  ("<<std::hex<<VX1730_BUFFER_ORGANIZATION<<") [hex]: 0x"<<std::hex<<data<<"  -  nsamples="<<std::dec<<data*10);  
  
    n_samples_per_buffer = data*10;
  }

  return n_samples_per_buffer;
}

/*!
 * \brief Lookup buffer length
 *
 * Contains the hardcoded mapping of the buffer length register value to the number of samples for a channel upon an acquisition trigger.
 *  - 0 : n_samples = 639990
 *  - 1 : n_samples = 319990
 *  - 2 : n_samples = 159990
 *  - 3 : n_samples = 79990
 *  - 4 : n_samples = 39990
 *  - 5 : n_samples = 19990
 *  - 6 : n_samples = 9990
 *  - 7 : n_samples = 4990
 *  - 8 : n_samples = 2550
 *  - 9 : n_samples = 1270
 *  - A : n_samples = 630
 *
 * \param[in] [code] [The register configuration value]
 * \return [the number of samples acquired for a single channel for that code]
 * \callgraph
 * \callergraph
 * \showrefby
 */
int vx1730::GetBufferLength( unsigned int code ){
  DEBUG("vx1730::GetBufferLength");

  int n_samples_per_buffer = 0;

  if(code==0x0)
    n_samples_per_buffer = 639990;
  else if(code==0x1)
    n_samples_per_buffer = 319990;
  else if(code==0x2)
    n_samples_per_buffer = 159990;
  else if(code==0x3)
    n_samples_per_buffer = 79990;
  else if(code==0x4)
    n_samples_per_buffer = 39990;
  else if(code==0x5)
    n_samples_per_buffer = 19990;
  else if(code==0x6)
    n_samples_per_buffer = 9990;
  else if(code==0x7)
    n_samples_per_buffer = 4990;
  else if(code==0x8)
    n_samples_per_buffer = 2550;
  else if(code==0x9)
    n_samples_per_buffer = 1270;
  else if(code==0xA)
    n_samples_per_buffer = 630;

  return n_samples_per_buffer;
}

/*!
 * \brief Set the threshold for an ADC channel
 *
 * Writes to the appropriate register to configure the specified ADC channel to have a given threshold.
 *
 * \param[in] [channel]   [Which channel is to be configured]
 * \param[in] [threshold] [The value of the threshold, in units of ADC counts, to be specified]
 *
 * \note If a value that is larger than 2^14 (14 bit dynamic range) is attempted to be written, then the maximum value will be written.
 * 
 * \callgraph
 * \callergraph
 * \showrefby
 */
void vx1730::SetTriggerChannelThreshold(int channel, unsigned int threshold){
  unsigned int threshold_bits = 0;
  
  if(threshold>std::pow(2,14)){
    threshold = std::pow(2,14)-1;
    WARNING("Trigger threshold over max, setting to : "<<threshold);
  }
  
  DEBUG("Setting trigger threshold : channel = "<<std::dec<<channel<<"  threshold = "<<threshold);
  ReadSlaveReg(m_crate,  m_base_address+VX1730_CHANNEL_TRIG_THRESH + (0x0100)*channel, threshold_bits, true);
  DEBUG("Current threshold = "<<threshold_bits);
  WriteSlaveReg(m_crate, m_base_address+VX1730_CHANNEL_TRIG_THRESH + (0x0100)*channel, threshold, true);
  ReadSlaveReg(m_crate,  m_base_address+VX1730_CHANNEL_TRIG_THRESH + (0x0100)*channel, threshold_bits, true);
  DEBUG("Set     threshold = "<<threshold_bits);
}

/*!
 * \brief Set the trigger group output width
 *
 * Sets the amount of time for which a given trigger group (e.g. channels{0,1}) will assert their trigger signal upon the ackowledgement within that group of a trigger given the thresholds and the logic that has been configured.  This is then transmitted to the LVDS outputs for that length of time as well as to the coincidence logic for self-triggering.
 *
 * \param[in] [group] [Which trigger group is to be configured]
 * \param[in] [width] [The desired output trigger signal width, in units of ns, to be configured for that group]
 *
 * \note If a value for the configured width is larger than the maximum of 20148, then an exception is thrown.
 * 
 * \callgraph
 * \callergraph
 * \showrefby
 * \todo Instead of an exception, why not just send a WARNING and set the maximum value?
 */
void vx1730::SetTriggerGroupOutputWidth(int group, unsigned int width){

  unsigned int trig_group_width;

  ReadSlaveReg(m_crate, m_base_address+VX1730_CHANNEL_TRIG_PULSE_WIDTH+(0x0100*group), trig_group_width);
  DEBUG("TriggerGroup PulseWidth Initial : Grp="<<group<<"  Width="<<trig_group_width);

  int set_width_ns = 32;
  unsigned int set_width_clocks = 4;

  set_width_ns = width;

  if(set_width_ns<0){
    THROW(DigitizerHardwareException, "The desired output trigger pulse must be positive but in the config it is negative : "+to_string(set_width_ns));
  }
  else if(set_width_ns>2048){
    THROW(DigitizerHardwareException, "The desired output trigger pulse width is too large (maximum width is 2048 ns = 256 clock cycles) : "+to_string(set_width_ns));
  }
  else{
    set_width_clocks = floor(set_width_ns/8.0);
  }

  DEBUG("Setting trigger output width : t="<<set_width_ns<<"  clocks="<<set_width_clocks);

  for(int iBit=0; iBit<8; iBit++){
    SetWordBit(trig_group_width, iBit, 0);
  }

  trig_group_width |= set_width_clocks;

  WriteSlaveReg(m_crate, m_base_address+VX1730_CHANNEL_TRIG_PULSE_WIDTH+(0x0100*group), trig_group_width);
  ReadSlaveReg(m_crate, m_base_address+VX1730_CHANNEL_TRIG_PULSE_WIDTH+(0x0100*group), trig_group_width);
  DEBUG("TriggerGroup PulseWidth Final : "<<trig_group_width);    
}

/*!
 * \brief Set the dynamic range for an ADC channel
 *
 * Writes to the appropriate register to configure the dynamic range with which a given ADC acquires data.  This dynamic range is limited to be either 0.5 or 2.0 volts in the hardware.
 *
 * \param[in] [channel]   [Which channel is to be configured]
 * \param[in] [dynamic_range] [The value of the dynamic range, either 0.5 or 2.0, to be used for digitization.]
 *
 * \note If a value that is not one of the limited number of choices, an exception is thrown.
 * 
 * \callgraph
 * \callergraph
 * \showrefby
 * \todo Instead of an exception, just have it select a default value.
 */
void vx1730::SetChannelDynamicRange(int channel, double dynamic_range){

  // dynamic range
  unsigned int dynamic_range_bit;
  ReadSlaveReg(m_crate, m_base_address+VX1730_CHANNEL_DYNAMIC_RANGE+(0x0100)*channel, dynamic_range_bit);
  if( dynamic_range==0.5 ){
    SetWordBit(dynamic_range_bit, 0, 1);
  }
  else if( dynamic_range==2.0 ){
    SetWordBit(dynamic_range_bit, 0, 0);
  }
  else{
    THROW(DigitizerHardwareException, "Not a valid choice for a dynamic range to set : "+to_string(dynamic_range));
  }
  WriteSlaveReg(m_crate, m_base_address+VX1730_CHANNEL_DYNAMIC_RANGE+(0x0100)*channel, dynamic_range_bit);
}

/*!
 * \brief Set the DC offset for an ADC channel
 *
 * Writes to the appropriate register to configure the DC offset used when a given ADC acquires data.  The DC offset is provided in volts.
 *
 * \param[in] [channel]   [Which channel is to be configured]
 * \param[in] [dc_offset] [The desired DC offset, in volts, for the given channel]
 *
 * \note If the DC offset is greater than the dynamic range, an exception is thrown.
 * 
 * \callgraph
 * \callergraph
 * \showrefby
 * \todo Instead of an exception, just have it select a default value.
 */
void vx1730::SetChannelDCOffset(int channel, double dc_offset){

  unsigned int dc_offset_bits;
  unsigned int channelStatus;
  double set_dynamic_range=2.0; 
  unsigned int data;
  
  ReadSlaveReg(m_crate, m_base_address+VX1730_CHANNEL_STATUS+(0x0100)*channel, data, true );
  DEBUG("CheckDCRead : "<<ConvertIntToWord(data));
  
  ReadSlaveReg(m_crate, m_base_address+VX1730_CHANNEL_DYNAMIC_RANGE+(0x0100)*channel, data);
  if( GetWordBit(data,0)==0 ){
    set_dynamic_range=2.0;
  }
  else if( GetWordBit(data,0)==1 ){
    set_dynamic_range=0.5;
  }
  else{
    THROW(DigitizerHardwareException, "Not a valid choice for a dynamic range when inspected : "+to_string(GetWordBit(data,0)));
  }

  if(abs(dc_offset)>set_dynamic_range){
    THROW(DigitizerHardwareException, "This DC offset is too large : "+to_string(abs(dc_offset)));
  }
  
  dc_offset_bits = (unsigned int)(floor(((dc_offset + (set_dynamic_range/2.0))/set_dynamic_range) * 65536)-1.0);
  DEBUG("DC Offset Bits: "<<std::dec<<dc_offset_bits);
  
  // first check 0x1n88
  ReadSlaveReg(m_crate, m_base_address+VX1730_CHANNEL_STATUS+(0x0100*channel), channelStatus, true );
  DEBUG("Status (bit [2] must be 0) : "<<ConvertIntToWord(channelStatus));
  
  // write and check the offset
  WriteSlaveReg(m_crate, m_base_address+VX1730_CHANNEL_DAC+(0x0100*channel), dc_offset_bits, true );
  ReadSlaveReg(m_crate, m_base_address+VX1730_CHANNEL_DAC+(0x0100*channel), dc_offset_bits, true );
  
  ReadSlaveReg(m_crate, m_base_address+VX1730_CHANNEL_STATUS+(0x0100)*channel, data, true );
  DEBUG("CheckDCRead : "<<ConvertIntToWord(data));

}
