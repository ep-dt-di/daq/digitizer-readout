/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/

/*!
 * \file Helper.h
 *
 * \author Sam Meehan
 * \date 13 Jan 2021
 *
 * \brief Helper utilities for code
 *
 * These are additional utilities, not necessarily specific to the digitizer, which are used 
 * throughout.  For example, there are utilities that cast words into more interpretable strings.
 */
 
#ifndef  HELPER_INCLUDE_H
#define  HELPER_INCLUDE_H

#include "project_system_define.h"      //define LINUX or WINDOWS
#include "project_interface_define.h"   //define Interface (sis1100/sis310x, sis3150usb or Ethnernet UDP)
#include "vme_interface_class.h"
#include "sis3153usb.h"
#include "sis3153ETH_vme_class.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <cmath>

#include <signal.h>

#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <ctime>
#include <chrono>
using namespace std::chrono;

#include "nlohmann/json.hpp"
// for convenience
using json = nlohmann::json;



// only needed for standalone compilation, otherwise you want to use the real logging in daqling
#include "Logging.hpp"

// for exception construction
#include "Exceptions/Exceptions.hpp"

/*!
 * \class DigitizerHardwareException
 * \copyright Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
 * \author Sam Meehan
 * \date 13 January 2021
 *
 * \brief To allow for digitizer specific exception handline
 *
 */
class DigitizerHardwareException : public Exceptions::BaseException { 
  using Exceptions::BaseException::BaseException; 
};



void printHelp();

json openJsonFile(std::string filepath);

std::string ConvertIntToWord(unsigned int word, bool debug=false);

void SetWordBit(unsigned int& word, int bit_location, int bit_val);

int GetWordBit(unsigned int word, int bit_location);

std::string GetDateVerboseString();

void Wait(float nseconds);

std::string ConvertDecToHexString(int input);

// stuff for parsing the IP address
bool IsNumber(char input);
bool IsDot(char input);
bool IsIPAddress(const char *input);
std::string GetIPAddress(const char* input);

#endif




