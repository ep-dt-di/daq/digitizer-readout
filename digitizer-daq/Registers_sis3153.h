/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/

/*!
 * \file Registers_sis3153.h
 * \copyright Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
 * \author Sam Meehan
 * \date 13 Jan 2021
 *
 * \brief Registers for the interface board
 *
 * Documents the registers to which read/writes can be performed on the interface board.
 * More information should be read from the manufacturer documentation (albeit sparse) :
 *  - sis3153 Manual : [sis3153-m-usb-1-v106-manual.pdf](https://faserdaq.web.cern.ch/faserdaq/sis3153-m-usb-1-v106-manual.pdf)
 *  - sis3153 Ethernet Addendum : [sis3153-m-eth-1-v107-ethernet-addendum.pdf](https://faserdaq.web.cern.ch/faserdaq/sis3153-m-eth-1-v107-ethernet-addendum.pdf)
 */

#ifndef  REGISTERS_SIS3153_INCLUDE_H
#define  REGISTERS_SIS3153_INCLUDE_H

/*!
 * \brief Control Status
 *
 * USB control and status
 * Mode : Read/Write
 */
#define SIS3153_CONTROL_STATUS              0x0    
/*!
 * \brief Board Version
 *
 * Module Id. and firmware version register
 * Mode : Read
 */
#define SIS3153_MODID_VERSION               0x1
/*!
 * \brief Serial Number
 *
 * Board serial number
 * Mode : Read
 */
#define SIS3153_SERIAL_NUMBER_REG           0x2
/*!
 * \brief LEMO Control
 *
 * LEMO I/O control
 * Mode : Read/Write
 */
#define SIS3153_LEMO_IO_CTRL_REG            0x3
/*!
 * \brief Crate Master
 *
 * VME Master Status/Control
 * Mode : Read/Write
 */
#define SIS3153_VME_MASTER_CONTROL_STATUS   0x10
/*!
 * \brief Master Cycle
 *
 * VME Master cycle Status
 * Mode : Read
 */
#define SIS3153_VME_MASTER_CYCLE_STATUS     0x11
/*!
 * \brief Interrupt
 *
 * VME Interrupt Status
 * Mode : Read
 */
#define SIS3153_VME_INTERRUPT_STATUS        0x12
/*!
 * \brief Board Reset
 *
 * Full board reset to defaults
 */
#define SIS3153_KEY_RESET_ALL               0x0100

#endif