/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/

/*!
 * \file Registers_vx1730.h
 * \copyright Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
 * \author Sam Meehan
 * \date 13 Jan 2021
 *
 * \brief Registers for the interface board
 *
 * Verbose variables used to refer to specific registers for the vx1730 digitizer board.
 * Full documentation can be found in the <a href="https://www.caen.it/products/vx1730/">UM5118_725-730__Registers_Description_rev4.pdf<a>
 * manual on the CAEN website linked here. 
 *
 * If you need to know what a specific bit is used for, then you will need to refer to the PDF documentation.
 */
 
#ifndef  REGISTERS_VX1730_INCLUDE_H
#define  REGISTERS_VX1730_INCLUDE_H

/*!
 * \brief Event Readout Buffer
 *
 * This is the addressing space for the event readout. The event payload is made of 32‐bit words; its structure is defined in the User Manual of the board.
 *
 * Mode : Read
 */
#define VX1730_EVENT_READOUT_BUFFER            0x0000
/*!
 * \brief Dummy32
 *
 * Writing and reading at this register can be used for debug purposes to test the local bus.
 *
 * Mode : Read/Write
 */
#define VX1730_CHANNEL_DUMMY32                 0x1024    
/*!
 * \brief AMC Firmware Revision
 *
 * This register contains the channel FPGA (AMC) revision information. The complete format is:
 * Firmware Revision = X.Y (16 lower bits)
 * Firmware Revision Date = Y/M/DD (16 higher bits)
 *
 * Mode : Read
 *
 * \note EXAMPLE 1: revision 1.03, November 12th, 2007 is 0x7B120103.
 * \note EXAMPLE 2: revision 2.09, March 7th, 2016 is 0x03070209.
 * \note the nibble code for the year makes this information to roll over each 16 years.
 * \note For channel 0 only. To read/write to other channels you must change the 0 to the specific channel number
 */
#define VX1730_CHANNEL_AMC_FIRMWARE_VERSION    0x108C    
/*!
 * \brief DC Offset
 *
 * This register allows to adjust the baseline position (i.e. the 0 Volt) of the input signal on the ADC scale. The ADC scale ranges from 0 to 2NBit ‐ 1, where NBit is the number of bits of the on‐board ADC. The DAC controlling the DC Offset has 16 bits, i.e. it goes from 0 to 65535 independently from the NBit value and the board type.
 * Typically a DC Offset value of 32K (DAC mid‐scale) corresponds to about the ADC mid‐scale. Increasing values of DC Offset make the baseline decrease. The range of the DAC is about 5% (typ.) larger than the ADC range, hence DAC settings close to 0 and 64K correspond to ADC respectively over and under range.
 *
 * Mode : Read/Write
 *
 * \note For channel 0 only. To read/write to other channels you must change the 0 to the specific channel number
 * \warning before writing this register, it is necessary to check that bit[2] = 0 at 0x1n88, otherwise the writing process will not run properly! After writing, the user is recommended to wait for few seconds before a new RUN to let the DAC output (i.e. the new programmed DC offset) get stabilized.
 */
#define VX1730_CHANNEL_DAC                     0x1098    
/*!
 * \brief Channel n ADC Temperature
 *
 * This register monitors the temperature of the ADC chips.
 *
 * \note If the temperature varies significantly during the digitizer operation, the user is recommended to perform a new channel calibration procedure (see register 0x809C) to restore the board performance. This is not true in case of 725S and 730S as these models do not require any calibration.
 * \note For channel 0 only. To read/write to other channels you must change the 0 to the specific channel number
 */
#define VX1730_CHANNEL_TEMPERATURE             0x10A8    
/*!
 * \brief Channel n Trigger Threshold
 *
 * The channel is able to generate a self‐trigger signal when the digitized input pulse exceeds a configurable threshold Vth. This register allows to set Vth individually for each channel.
 *
 * Mode : Read/Write
 *
 * \note For channel 0 only. To read/write to other channels you must change the 0 to the specific channel number
 */
#define VX1730_CHANNEL_TRIG_THRESH             0x1080    
/*!
 * \brief Channel n Pulse Width
 *
 * This register sets the width of the pulse generated when the input signal on the channel crosses the threshold. 
 * 
 * Mode : Read/Write 
 *
 * \note This setting is meaninful if bit[2] = 0 in the Self‐Trigger Logic register.
 * \note This register is implemented only from AMC FPGA firmware revision 0.2 on.
 *
 * \note For channel 0 only. To read/write to other channels you must change the 0 to the specific channel number
 */
#define VX1730_CHANNEL_TRIG_PULSE_WIDTH        0x1070    
/*!
 * \brief Couple n Self-Trigger Logic
 *
 * For a couple of channels of adjacent channels, this register sets the logic to generate the trigger request signal upon the self‐triggers from the two channels of the couple. Please, refer to the digitizer User Manual (Self‐Trigger section) for complete description.
 * In case of DT, NIM and 8‐channel VME boards: n = 0 (CH0‐CH1), 2 (CH2‐CH3), 4 (CH4‐CH5), 6 (CH6‐ Ch7). In case of 16‐channel VME boards, also n = 8 (CH8‐CH9), n = A (CH10‐CH11), n = C (CH12‐ CH13), n = E (CH14‐CH15) are admitted.
 *
 * Mote : Read/Write
 *
 * \note For channel 0 only. To read/write to other channels you must change the 0 to the specific channel number
 */
#define VX1730_CHANNEL_TRIG_LOGIC              0x1084    
/*!
 * \brief Channel n Status
 *
 * This register contains the status information of channel n.
 *
 * Mode : Read
 *
 * \note For channel 0 only. To read/write to other channels you must change the 0 to the specific channel number
 */
#define VX1730_CHANNEL_STATUS                  0x1088    
/*!
 * \brief Input Dynamic Range
 *
 * This register sets the input dynamic range of each channel individually.
 *
 * Mode : Read/Write
 *
 * \note For channel 0 only. To read/write to other channels you must change the 0 to the specific channel number
 */
#define VX1730_CHANNEL_DYNAMIC_RANGE           0x1028    
/*!
 * \brief Board Configuration [inclusive]
 *
 * This register contains general settings for the board configuration.
 *
 * Mode : Read/Write
 *
 * \note For channel 0 only. To read/write to other channels you must change the 0 to the specific channel number
 */
#define VX1730_CHANNEL_CONFIG                  0x8000
/*!
 * \brief Board Configuration [BitSet]
 *
 * This register contains general settings for the board configuration.
 *
 * Mode : Read/Write
 */
#define VX1730_CHANNEL_CFG_BIT_SET             0x8004
/*!
 * \brief Board Configuration [BitClear]
 *
 * This register contains general settings for the board configuration.
 *
 * Mode : Read/Write
 */
#define VX1730_CHANNEL_CFG_BIT_CLR             0x8008
/*!
 * \brief Buffer Organization
 *
 * Sets the number of buffers in which the channel memory can be divided. A write access to this register causes a soft‐ ware clear.
 * According to the BUFFER_CODE value written in the register, the number of buffers Nb is given by the BUFFER_CODE (see manual for mapping).
 * 
 * To obtain a number of samples per buffer (referring to one channel) different from the table above, it is necessary
 * to use the register address 0x8020. In this case, the BUFFER_CODE must be set to have the closest buffer size with a number of samples per buffer larger than the one set by 0x8020.
 * 
 * Mode : Read/Write
 *
 * \note EXAMPLE: to have a desired number of samples per buffer of 900 (set through the Custom Size register), the BUFFER_CODE must be 0x9 in case of 640‐kS/ch memory or 0xA if 5.12‐MS/ch one.
 * \warning IMPORTANT: for AMC FPGA firmware release < 0.2, the Size of one Buffer related to each Buffer Number is the number of the samples in the table without decreasing by 10.
 */
#define VX1730_BUFFER_ORGANIZATION             0x800C
/*!
 * \brief Custom Size
 *
 * Writing the number of memory locations per event (N_LOC) in this register, the user can set the record length, which is the number of samples (Ns) of the digitized waveform in the acquisition window.
 *
 * Mode : Read/Write
 *
 * \warning This register must not be written while acquisition is running.
 */
#define VX1730_CUSTOM_SIZE                     0x8020
/*!
 * \brief Channel n ADC Temperature
 *
 * This register monitors the temperature of the ADC chips.
 * 
 * Mode : Read
 *
 * \note If the temperature varies significantly during the digitizer operation, the user is recommended to perform a new channel calibration procedure (see register 0x809C) to restore the board performance. This is not true in case of 725S and 730S as these models do not require any calibration.
 */
#define VX1730_ADC_CALIBRATION                 0x809C
/*!
 * \brief Acquisition Control
 *
 * This register manages the acquisition settings.
 *
 * There are many specifications for single bit features and you are referred to the PDF documentation for these details.
 * 
 * Mode : Read/Write
 */
#define VX1730_ACQUISITION_CONTROL             0x8100
/*!
 * \brief Acquisition Status
 *
 * This register monitors a set of conditions related to the acquisition status.
 *
 * There are many specifications for single bit features and you are referred to the PDF documentation for these details.
 * 
 * Mode : Read
*/
#define VX1730_ACQUISITION_STATUS              0x8104
/*!
 * \brief Software Trigger
 *
 * Writing this register causes a software trigger generation which is propagated to all the enabled channels of the board.
 *
 * More : Write
 */
#define VX1730_SW_TRIGGER                      0x8108
/*!
 * \brief Global Trigger Mask
 *
 * This register sets which signal can contribute to the global trigger generation.
 *
 * There are many specifications for single bit features and you are referred to the PDF documentation for these details.
 *
 * Mode : Read/Write
 */
#define VX1730_TRIG_SRCE_EN_MASK               0x810C
/*!
 * \brief Front Panel TRG-OUT (GPO) Enable Mask
 *
 * This register sets which signal can contribute to generate the signal on the front panel TRG‐OUT LEMO connector (GPO in case of DT and NIM boards).
 * 
 * Mode : Read/Write
 */
#define VX1730_FP_TRIGGER_OUT_EN_MASK          0x8110
/*!
 * \brief Post Trigger
 *
 * The value of this register is used to set the number of post‐trigger samples, that is the number of further samples that are written by the FPGA in the channel memory, when a trigger occurs, before to freeze the buffer. The number of post trigger samples is:
 * Npost = PostTriggerValue*N + ConstantLatency
 * where:
 *   - Npost = number of post trigger samples.
 *   - PostTriggerValue = content of this register.
 *   - N = coefficient to be multiplied by the PostTriggerValue (N = 4 for 730 and N = 2 for 725).
 *   - ConstantLatency = constant number of samples added due to the latency associated to the trigger processing logic in the ROC FPGA. The value of this constant depends on the trigger source used and can change between different firmware revisions.
 *
 * Mode : Read/Write
 */
#define VX1730_POST_TRIGGER_SETTING            0x8114
/*!
 * \brief LVDS I/O Data
 *
 * This register allows to read out the logic level of the LVDS I/Os if the LVDS pins are configured as outputs, and to set the logic level of the LVDS I/Os if the pins are configured as inputs.
 *
 * Mode : Read/Write
 *
 * \note This register is supported by VME boards only.
 */
#define VX1730_FP_IO_DATA                      0x8118
/*!
 * \brief Front Panel I/O Control
 *
 * This register manages the front panel I/O connectors. Default value is 0x000000.
 *
 * There are many specifications for single bit features and you are referred to the PDF documentation for these details.
 *
 * Mode : Read/Write
 *
 */
#define VX1730_FP_IO_CONTROL                   0x811C
/*!
 * \brief Channel Enable Mask
 *
 * This register enables/disables selected channels to participate in the event readout. Disabled channels are not opera‐ tive.
 *
 * Mode : Read/Write
 *
 * \warning This register must not be modified while the acquisition is running.
 */
#define VX1730_CHANNEL_EN_MASK                 0x8120
/*!
 * \brief ROC FPGA Firmware Revision
 *
 *
 * This register contains the motherboard FPGA (ROC) firmware revision information. The complete format is:
 * Firmware Revision = X.Y (16 lower bits)
 * Firmware Revision Date = Y/M/DD (16 higher bits)
 * 
 * Mode : Read
 *
 * \note EXAMPLE 1: revision 3.08, November 12th, 2007 is 0x7B120308.
 * \note EXAMPLE 2: revision 4.09, March 7th, 2016 is 0x03070409.
 * \note The nibble code for the year makes this information to roll over each 16 years.
 */
#define VX1730_ROC_FPGA_FW_REV                 0x8124
/*!
 * \brief Event Stored
 *
 * This register contains the number of events currently stored in the Output Buffer.
 *
 * Mode : Read
 *
 * \note The value of this register cannot exceed the maximum number of available buffers according to the register address 0x800C.
 */
#define VX1730_EVENT_STORED                    0x812C
/*!
 * \brief Voltage Level Mode Configuration
 *
 * When the Voltage Level Mode is enabled (bit[2:0] = 100 (bin) of register 0x8144), this register sets the DAC value to be provided on the front panel MON/Sigma output LEMO connector: 1 LSB = 0.244 mV, terminated on 50 Ohm.
 *
 * Mode : Read/Write
 *
 * \note This register is supported by VME boards only.
 */
#define VX1730_SET_MONITOR_DAC                 0x8138
/*!
 * \brief Board Info
 *
 * This register contains the specific information of the board, such as the digitizer family, the channel memory size and the channel density.
 *
 * Mode : Read
 */
#define VX1730_BOARD_INFO                      0x8140
/*!
 * \brief Voltage Level Mode Configuration
 *
 * When the Voltage Level Mode is enabled (bit[2:0] = 100 (bin) of register 0x8144), this register sets the DAC value to be provided on the front panel MON/Sigma output LEMO connector: 1 LSB = 0.244 mV, terminated on 50 Ohm.
 *
 * Mode : Read/Write
 *
 * \note This register is supported by VME boards only.
 */
#define VX1730_MONITOR_MODE                    0x8144
/*!
 * \brief Event Size
 *
 * This register contains the current available event size in 32‐bit words. The value is updated after a complete readout of each event.
 *
 * Mode : Read
 */
#define VX1730_EVENT_SIZE                      0x814C
/*!
 * \brief Front Panel LVDS I/O New Features
 *
 * 
 * If the LVDS I/O new features are enabled (bit[8] = 1 of 0x811C), this register programs the functions of the front panel LVDS I/O 16‐pin connector. It is possible to configure the LVDS I/O pins by group of four (4).
 * Options are:
 *   - 1) 0000 = REGISTER, where the four LVDS I/O pins act as register (read/write according to the configured input/output option);
 *   - 2) 0001 = TRIGGER, where each group of four LVDS I/O pins can be configured to receive an input trigger for each channel (DPP Firmware only), or to propagate out the trigger request;
 *   - 3) 0010 = nBUSY/nVETO, where each group of four LVDS I/O pins can be configured as inputs (0 = nBusyIn, 1 = nVetoIn, 2 = nTrigger In, 3 = nRun In) or as outputs (0 = nBusy, 1 = nVeto, 2 = nTrigger Out, 3 = nRun );
 *   - 4) 0011 = LEGACY, that is to say according to the old LVDS I/O configuration (i.e. ROC FPGA firmware revisions lower than 3.8), where the LVDS can be configured as 0 = nclear TTT, and 1 = 2 = 3 = reserved in case of input LVDS setting, while they can be configured as 0 = Busy, 1 = Data ready, 2 = Trigger, 3 = Run in case of output LVDS setting.
 *
 * Please refer to the Front Panel LVDS I/Os section of the digitizer User Manual for detailed description.
 * 
 * Mode : Read/Write
 *
 * \note LVDS I/O new features are supported from ROC FPGA firmware revision 3.8 on. 
 * \note This register is supported by VME boards only.
 */
#define VX1730_FP_LVDS_IO_CTRL                 0x81A0
/*!
 * \brief Memory Buffer Almost Full Level
 *
 * This register allows to set the level for the Almost Full generation. The written value (ALMOST FULL LEVEL) represents the number of buffers that must be full of data before to assert the BUSY signal. This register takes part in the BUSY propagation among multiple boards.
 *
 * For the Almost Full description, please refer to the Acquisition Synchronization section of the digitizer User Manual.
 * 
 * Mode : Read/Write
 *
 * \note If this register is set to 0, the ALMOST FULL is a FULL.
 */
#define VX1730_ALMOST_FULL_LEVEL               0x816C
/*!
 * \brief Readout Control
 *
 * This register is mainly intended for VME boards, anyway some bits are applicable also for DT and NIM boards.
 *
 * There are many specifications for single bit features and you are referred to the PDF documentation for these details.
 *
 * Mode : Read/Write
 */
#define VX1730_VME_CONTROL                     0xEF00
/*!
 * \brief Readout Status
 *
 * This register contains information related to the readout.
 *
 * Mode : Read
 */
#define VX1730_VME_STATUS                      0xEF04
/*!
 * \brief Board ID
 *
 * The meaning of this register depends on which VME crate it is inserted in.
 * In case of VME64X crate versions, this register can be accessed in read mode only and it contains the GEO address of the module picked from the backplane connectors; when CBLT is performed, the GEO address will be contained in the Board ID field of the Event header (see the User Manual for further details).
 * In case of other crate versions, this register can be accessed both in read and write mode, and it allows to write the correct GEO address (default setting = 0) of the module before CBLT operation. GEO address will be contained in the Board ID field of the Event header (see the User Manual for further details).
 * 
 * Mode : Read/Write
 *
 *  \note This register is supported by VME boards only.
 */
#define VX1730_BOARD_ID                        0xEF08
/*!
 * \brief MCST Base Address and Control
 *
 * This register configures the board for the VME Multicast Cycles. 
 *
 * Mode : Read/Write
 *
 * \note This register is supported by VME boards only.
 */
#define VX1730_MULTICAST_BASE_ADDCTL           0xEF0C
/*!
 * \brief Relocation Address
 *
 * If address relocation is enabled through register 0xEF00 (bit[6] = 1), this register sets the VME Base Address of the module.
 *
 * Mode : Read/Write
 *
 * \note This register is supported by VME boards only.
 */
#define VX1730_RELOC_ADDRESS                   0xEF10
/*!
 * \brief Interrupt Status/ID
 *
 * This register contains the STATUS/ID that the module places on the VME data bus during the Interrupt Acknowledge cycle.
 * 
 * Mode : Read/Write
 *
 * \note This register is supported by VME boards only.
 */
#define VX1730_INTERRUPT_STATUS_ID             0xEF14
/*!
 * \brief Interrupt Event Number
 *
 * This register sets the number of events that causes an interrupt request. If interrupts are enabled, the module gener‐ ates a request whenever it has stored in memory a Number of Events > INTERRUPT EVENT NUMBER.
 *
 * Mode : Read/Write
 */
#define VX1730_INTERRUPT_EVT_NB                0xEF18
/*!
 * \brief Max Number of Events per BLT
 *
 * This register sets the maximum number of complete events which has to be transferred for each block transfer (via VME BLT/CBLT cycles, or block readout through USB or Optical Link).
 * 
 * Mode : Read/Write
 */
#define VX1730_BLT_EVENT_NB                    0xEF1C
/*!
 * \brief Scratch
 *
 * This register can be used to write/read words for test purposes.
 *
 * Mode : Read/Write
 */
#define VX1730_SCRATCH                         0xEF20
/*!
 * \brief Software Reset
 *
 * All the digitizer registers can be set back to their default values on software reset command by writing any value at this register, or by system reset from backplane in case of VME boards.
 *
 * Mode : Write
 */
#define VX1730_SW_RESET                        0xEF24
/*!
 * \brief Software Clear
 *
 * All the digitizer internal memories are cleared:
 *   - automatically by the firmware at the start of each run;
 *   - on software command by writing at this register;
 *   - by hardware (VME boards only) through the LVDS interface properly configured.
 *
 * A clear command does not change the registers actual value, except for resetting the following registers: ‐ Event Stored;
 *   - Event Size;
 *   - Channel / Group n Buffer Occupancy.
 *
 * This register resets also the trigger time stamp.
 * 
 * Mode : Write
 */
#define VX1730_SW_CLEAR                        0xEF28
/*!
 * \brief Configuration Reload
 *
 * A write access of any value at this location causes a software reset, a reload of Configuration ROM parameters and a PLL reconfiguration.
 *
 * Mode : Write
 */
#define VX1730_CONFIG_RELOAD                   0xEF34
/*!
 * \brief Configuration ROM Checksum
 *
 * This register contains information on 8‐bit checksum of Configuration ROM space.
 * 
 * Mode : Read
 */
#define VX1730_CONFIG_ROM                      0xF000
/*!
 * \brief Configuration ROM Board Version
 *
 * This register contains the board version information.
 *
 * Read the PDF documentation for the detailed value mapping.
 *
 * Mode : Read
 */
#define VX1730_CONFIG_ROM_BOARD_VERSION        0xF030
/*!
 * \brief Configuration ROM Board Form Factor
 *
 * This register contains the information of the board form factor.
 *
 * Read the PDF documentation for the detailed value mapping.
 *
 * Mode : Read
 */
#define VX1730_CONFIG_ROM_BOARD_FORMFACTOR     0xF034

#endif